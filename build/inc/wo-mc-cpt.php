<?php 
/**
 * Creating the Arabic Daily Testimonials
 */
function ad_mc_cpt() {
  $labels = array(
    'name'               => _x( 'Email Optins', 'post type general name' ),
    'singular_name'      => _x( 'Email Optin', 'post type singular name' ),
    'add_new'            => _x( 'Add New', 'ad-email-optin' ),
    'add_new_item'       => __( 'Add New Email Optin' ),
    'edit_item'          => __( 'Edit Email Optin' ),
    'new_item'           => __( 'New Email Optin' ),
    'all_items'          => __( 'All Email Optins' ),
    'view_item'          => __( 'View Email Optin' ),
    'search_items'       => __( 'Search Email Optins' ),
    'not_found'          => __( 'No Email Optins found' ),
    'not_found_in_trash' => __( 'No Email Optins found in the Trash' ), 
    'parent_item_colon'  => '',
    'menu_name'          => 'Email Optins',
  );
    
  $args = array(
    'labels'        => $labels,
    'description'   => 'Arabic Daily Email Optins',
    'public'        => true,
    'supports'		=>	array( 'title' ),
    'menu_position' => 20,
    'menu_icon'     => 'dashicons-welcome-add-page',  
    'has_archive'   => false,
    'hierarchical'  => false,
    'publicly_queryable' => false,
  );
  register_post_type( 'ad-mcoptin', $args ); 
}
add_action( 'init', 'ad_mc_cpt' );