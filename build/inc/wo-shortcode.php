<?php

	
add_shortcode( 'fullscreenform', 'ad_fullscreen_form_popup' );
function ad_fullscreen_form_popup ( $atts ) {
    $vars = shortcode_atts( array(
            'id' => '',
            'title' => 'false',
            'description' => 'false',
            'ajax' => 'true', 
            'tabindex' => '',
            'button' => 'Launch Form'
    ), $atts );
    
    $form = gravity_form( $vars['id'], $vars['title'], $vars['description'], false, null, $vars['ajax'], $vars['tabindex'], false );
    
    $script = '<script>  jQuery(document).ready(function() {
        jQuery(".open-fullscreen-form").magnificPopup({
            type: "inline",
            midclick: "true"
        });
    });
    </script>';
    
    $output = '<div id="fullscreen-form-popup-' . $vars['id'] . '" class="fullscreen-form mfp-hide">' . $form . '</div><div class="open-form-button-wrapper"><a href="#fullscreen-form-popup-' . $vars['id'] . '" class="open-fullscreen-form">' . $vars['button'] . '</a></div>' . $script;
    
    return $output;

}

