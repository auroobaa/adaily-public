<?php
/**
 *
 * @package ArabicDaily_Theme
 */

get_header(); ?>
<?php $postid = get_option( 'page_for_posts' ); ?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
           <?php if (get_field('hide_page_title', $postid) == false) : ?>
                <header class="page-builder-header">
                   <h1 class="entry-title"><?php echo get_the_title($postid); ?></h1>
                </header><!-- .entry-header -->
            <?php endif; ?>
            <?php
                // check if the flexible content field has rows of data
                if( have_rows('components', $postid) ):
                    // loop through the rows of data
                    while ( have_rows('components', $postid) ) : the_row();

                        if( get_row_layout() == 'text_editor' ):

                            get_template_part( 'builder/builder', 'text' );

                        elseif( get_row_layout() == 'call_to_action' ):

                            get_template_part( 'builder/builder', 'cta' );

                        elseif( get_row_layout() == 'faq' ):

                            get_template_part( 'builder/builder', 'faq' );           

                        elseif( get_row_layout() == 'multimedia' ):

                            get_template_part( 'builder/builder', 'multimedia' );   

                        elseif( get_row_layout() == 'full_width_media' ):
            
                            get_template_part( 'builder/builder', 'fullwidth' );  
            
                        elseif( get_row_layout() == 'courses_block' ):
            
                            get_template_part( 'builder/builder', 'courses' );  

                        elseif( get_row_layout() == 'email_sign_up' ):
            
                            get_template_part( 'builder/builder', 'mailchimp' ); 
            
                        elseif( get_row_layout() == 'blog_posts' ):
            
                            get_template_part( 'builder/builder', 'posts' );  
            
                        elseif( get_row_layout() == 'testimonials' ):
            
                            get_template_part( 'builder/builder', 'testimonials' );  

                        elseif( get_row_layout() == 'note' ):
            
                            get_template_part( 'builder/builder', 'note' );  
            
                        elseif( get_row_layout() == 'tiles' ):
            
                            get_template_part( 'builder/builder', 'tiles' );  
            
                        elseif( get_row_layout() == 'column_content' ):
            
                            get_template_part( 'builder/builder', 'columncontent' );  
            
                        elseif( get_row_layout() == 'long_form' ):
            
                            get_template_part( 'builder/builder', 'longform' );
            
                        elseif( get_row_layout() == 'conditional_call_to_action' ):
            
                            get_template_part( 'builder/builder', 'conditionalcta' );  

                        endif;

                    endwhile;

                else :

                    // no layouts found

                endif; 


                ?>

            </section>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
