<?php
/**
 * Template Name: Dashboard
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package ArabicDaily_Theme
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
            <?php if (!is_user_logged_in()) : ?>
            <div class="if-not-logged-in">
                <?php echo do_shortcode('[theme-my-login]'); ?>
            </div>
            <?php endif; ?>
			<?php
			while ( have_posts() ) : the_post(); ?>
				<article id="ld-profile-page">
                    <header class="entry-header">
                        <?php
                            if ( is_single() ) {
                                the_title( '<h1 class="entry-title">', '</h1>' );
                            } else {
                                the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
                            }

                        if ( 'post' === get_post_type() ) : ?>
                        <div class="entry-meta">
                            <?php arabicdaily_posted_on(); ?>
                        </div><!-- .entry-meta -->
                        <?php
                        endif; ?>
                    </header><!-- .entry-header -->

                    <div class="entry-content">
                        <?php
                            the_content( sprintf(
                                wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'arabicdaily' ), array( 'span' => array( 'class' => array() ) ) ),
                                the_title( '<span class="screen-reader-text">"', '"</span>', false )
                            ) ); 
                        ?>
                    </div><!-- .entry-content -->

                    <footer class="entry-footer">
                        <?php arabicdaily_entry_footer(); ?>
                    </footer><!-- .entry-footer -->
                </article><!-- #post-## -->
			<?php endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
