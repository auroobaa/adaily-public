<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package ArabicDaily_Theme
 */

?>
<?php 

$media = get_field('media', 'option');
$heading = get_field('heading', 'option');
$text = get_field('text', 'option');
$hide = get_field('hide_option', 'option');
$course_id = get_the_ID();
$user_id = get_current_user_id();
$has_access = sfwd_lms_has_access( $course_id, $user_id );
//echo '<pre>' . var_export($has_access, true) . '</pre>';
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php //the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header><!-- .entry-header -->

	<div class="entry-content">

        <?php if (get_field('welcome_video') && $has_access) : ?>
        <section class="multimedia welcome-video">

            <span class="hidden-reveal"><i class="fa fa-angle-double-right"></i>View <?php echo $heading; ?></span>

            <div class="builder total-wrapper">
                <div class="content-wrapper">  
                <?php if ($hide) : ?>
                     <span class="hide"><i class="fa fa-times"></i> HIDE</span>
                 <?php endif; ?>
                  <div class="content">
                       <h2><?php echo $heading; ?></h2>
                       <?php echo $text; ?>
                   </div>
                </div>
                <div class="media">
                    <?php echo $media; ?>
                </div>
            </div>
        </section>
        <script>
            var MI = { 
                hideButton: function() {
                    jQuery('.hide').click( MI.hideWrapper );  
                },

                hideWrapper: function(event) {
                    jQuery('.total-wrapper').slideUp( 500 );
                    MI.showRevealer();
                },

                showRevealer: function(event) {
                    jQuery('.hidden-reveal').fadeIn( 500 );
                }

            };

            var HI = { 
                revealerButton: function() {
                    jQuery('.hidden-reveal').click( HI.hideRevealer);  
                },

                showWrapper: function(event) {
                    jQuery('.total-wrapper').slideDown( 500 );
        //            HI.hideRevealer();
                },

                hideRevealer: function(event) {
                    jQuery('.hidden-reveal').fadeOut( 500 );
                    HI.showWrapper();
                }

            };

            jQuery(document).ready(HI.revealerButton);
            jQuery(document).ready(MI.hideButton);

        </script> 
        <?php endif; ?>
        
        
		<?php
			the_content();

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'arabicdaily' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php
			edit_post_link(
				sprintf(
					/* translators: %s: Name of current post */
					esc_html__( 'Edit %s', 'arabicdaily' ),
					the_title( '<span class="screen-reader-text">"', '"</span>', false )
				),
				'<span class="edit-link">',
				'</span>'
			);
		?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
