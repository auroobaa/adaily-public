<?php
/**
 * Displays a user's profile.
 * 
 * Available Variables:
 * 
 * $user_id 		: Current User ID
 * $current_user 	: (object) Currently logged in user object
 * $user_courses 	: Array of course ID's of the current user
 * $quiz_attempts 	: Array of quiz attempts of the current user
 * $shortcode_atts 	: Array of values passed to shortcode
 * 
 * @since 2.1.0
 * 
 * @package LearnDash\User
 */
?>
<?php
	global $learndash_assets_loaded;
	if ( !isset( $learndash_assets_loaded['scripts']['learndash_template_script_js'] ) ) {
		$filepath = SFWD_LMS::get_template( 'learndash_template_script.js', null, null, true );
		if ( !empty( $filepath ) ) {
			wp_enqueue_script( 'learndash_template_script_js', learndash_template_url_from_path( $filepath ), array( 'jquery' ), LEARNDASH_SCRIPT_VERSION_TOKEN, true );
			$learndash_assets_loaded['scripts']['learndash_template_script_js'] = __FUNCTION__;

			$data = array();
			$data['ajaxurl'] = admin_url('admin-ajax.php');
			$data = array( 'json' => json_encode( $data ) );
			wp_localize_script( 'learndash_template_script_js', 'sfwd_data', $data );
		}
	}
	LD_QuizPro::showModalWindow();
?>
<div id="learndash_profile">

<!--
    <div class="expand_collapse">
        <a href="#" onClick='return flip_expand_all("#course_list");'><?php _e( 'Expand All', 'learndash' ); ?></a> | <a href="#" onClick='return flip_collapse_all("#course_list");'><?php _e( 'Collapse All', 'learndash' ); ?></a>
    </div>
-->

	<div class="learndash_profile_heading">
		<h2><?php _e( 'Profile', 'learndash' ); ?></h2>
	</div>

	<div class="profile_info clear_both">
		<div class="profile_avatar">
			<?php echo get_avatar( $current_user->user_email, 96 ); ?>
			<div class="profile_edit_profile" align="center">
                <a href='<?php echo wc_customer_edit_account_url(); ?>'><?php _e( 'Edit profile', 'learndash' ); ?></a>
            </div>
        </div>

		<div class="learndash_profile_details">
			<?php if ( ( ! empty( $current_user->user_lastname) ) || ( ! empty( $current_user->user_firstname ) ) ): ?>
				<div><b><?php _e( 'Name', 'learndash' ); ?>:</b> <?php echo $current_user->user_firstname . ' ' . $current_user->user_lastname; ?></div>
			<?php endif; ?>
			<div><b><?php _e( 'Username', 'learndash' ); ?>:</b> <?php echo $current_user->user_login; ?></div>
			<div><b><?php _e( 'Email', 'learndash' ); ?>:</b> <?php echo $current_user->user_email; ?></div>
		</div>
	</div>
	<div class="ad-woocommerce">
        <?php echo do_shortcode('[woocommerce_my_account]'); ?>
    </div>
	<div class="learndash_profile_heading no_radius clear_both learndash-course-list-heading">
		<h2 class="ld_profile_course"><?php printf( _x( 'Registered %s', 'Registered Courses Label', 'learndash' ), LearnDash_Custom_Label::get_label( 'courses' ) ); ?></h2>
	</div>
	<div id="course-list">

		<?php if ( ! empty( $user_courses ) ) : ?>

			<?php foreach ( $user_courses as $course_id ) : ?>
				<?php
                    $course = get_post( $course_id);

                    $course_link = get_permalink( $course_id );

                    $progress = learndash_course_progress( array(
                        'user_id'   => $user_id,
                        'course_id' => $course_id,
                        'array'     => true
                    ) );

                    $status = ( $progress['percentage'] == 100 ) ? 'completed' : 'notcompleted';
				?>
				<div class="registered-courses" id='ad-course-<?php echo esc_attr( $user_id ) . '-' . esc_attr( $course->ID ); ?>'>

                    <div class="ad-learndash-course-link"><a href="<?php echo esc_attr( $course_link ); ?>"><?php echo $course->post_title; ?></a></div>
                    <div class="ad-learndash-course-progress">
                        <?php echo sprintf( __( '%s%% Complete', 'learndash' ), $progress['percentage'] ); ?>
                    </div>

				</div>
			<?php endforeach; ?>
        <?php else : ?>
            <p>You aren't registered for any courses, yet.</p>
		<?php endif; ?>

	</div>
</div>
<?php if ( apply_filters('learndash_course_steps_expand_all', $shortcode_atts['expand_all'], 0, 'profile_shortcode' ) ) { ?>
	<script>
		jQuery(document).ready(function() {
			jQuery("#learndash_profile .list_arrow").trigger('click');
		});
	</script>	
<?php } ?>
