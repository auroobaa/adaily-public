<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package ArabicDaily_Theme
 */

?>

	</div><!-- #content -->
	<footer id="colophon" class="site-footer" role="contentinfo">
        <section class="site-footer-hero">
            <p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 403.2 244.7" style="enable-background:new 0 0 403.2 244.7;" xml:space="preserve">
                <style type="text/css">
                    .st0{fill:#3F3F3F;}
                    .st1{font-family:'Halant'; font-weight: normal;}
                    .st2{font-size:97.9693px;}
                    .st3{font-family:'Halant'; font-weight: 500;}
                </style>
                <g>
                    <g>
                        <path class="st0" d="M25.4,36.7c-1.1-5.2,1.1-8.6,7.4-10.7c1.8-0.6,3.6-1,5.5-1.4c22.1-5.4,43.8-7.8,64.4,7.6
                            c-1.4-2.6-2.2-4.9-3.6-6.7C86.2,9.8,69.2,3.7,49.6,6c-8.5,1-14.8,3-19.7,6.4C21.6,21.7,24.7,34.5,25.4,36.7z"/>
                        <path class="st0" d="M82.7,72.1c-13.6,4.1-27.2,1.8-38.8-6.9C35.7,59.1,31,50.2,26.8,41c-0.7-1.5-1.2-2.9-1.5-4.2
                            c-0.6-2.2-3.8-15,4.5-24.4c-5.9,4.1-9.7,10.4-12.4,19.8c-5.1,17.5,5.8,40.1,20.2,49.9c3.5,2.4,7,4.8,10.1,7
                            c7.1-5.9,27.4-14,36-17.3C83.4,71.9,83,72,82.7,72.1z"/>
                        <path class="st0" d="M44.6,109.2c4.6-3.2,7.8-6.3,11.7-8C75,92.8,93.5,84,112.6,77c9.6-3.5,15-9.2,18-18.3
                            c0.4-1.1,0.1-2.5,0.1-3.7c-1,0.2-2,0.2-2.9,0.5c-14.6,5.5-29.1,11.7-44,16.3c-8.6,3.2-28.9,11.3-36,17.3c-2.9,3.3-5.7,6.6-8.5,9.9
                            C35.9,106.5,43.8,108.9,44.6,109.2z"/>
                        <path class="st0" d="M159,155.9c-0.2-2.8-0.6-5.5-1.1-8.1c2,19.3-10,34.2-13.1,37.7c-10.1,13.2-23.5,21.9-38.2,28.9
                            c-13.8,6.6-28.4,8.3-43.5,5.4c-11.9-2.3-23-6.5-32.7-14.1c-8.8-6.9-15.9-15.6-17.2-26.5c-1-8.8,0.5-18.4,2.9-27.1
                            c3.7-13.7,9.1-27.1,18.2-38.4c0.4-0.5,0.9-0.9,1-1c-1.1-1.1-3-4.5,2.9-12.5c-10,12.1-19,24.8-24.9,39.7
                            c-6.9,17.3-9.3,35.5-5.4,53.4c5.2,23.8,23,36.6,45.2,42.9c16,4.6,32,2.8,47.9-2.8c9-3.2,17.2-7.5,24.5-13.2
                            c6.5-5,12.1-11.1,17.5-17.3C154.7,189.5,160.3,174,159,155.9z"/>
                        <path class="st0" d="M35.3,112.6L35.3,112.6c7.3,4.9,13.9,10.6,21.5,14.3c6.7,3.2,14.4,4.6,21.8,5.9c17.1,2.8,34.5,4.4,51.5,7.8
                            c9.2,1.9,17.4,7.4,21.4,16.5c4.6,10.4,0.1,19.2-6.2,27.7c-0.2,0.2-0.4,0.5-0.5,0.7c3.1-3.5,15.1-18.3,13.1-37.7
                            c-3-13.3-11-22.4-25.6-26c-11.8-3-24.1-4.1-36.2-5.6c-15.3-1.9-30.6-3.3-45.9-5.1c-1.4-0.2-2.7-0.9-5.5-1.9l0,0
                            c-0.8-0.2-8.7-2.6-5.4-10.2c-0.3,0.4-0.7,0.8-1,1.2C32.3,108.1,34.1,111.5,35.3,112.6z"/>
                    </g>
                    <g>
                        <g>
                            <text transform="matrix(1 0 0 1 144.0639 96.8312)" class="st0 st1 st2">Arabic</text>
                            <text transform="matrix(1 0 0 1 173.2084 186.3546)" class="st0 st3 st2">Daily</text>
                        </g>
                    </g>
                </g>
                </svg>
            </a></p>
        
            <nav class="footer-social" role="navigation">
                <?php 
                wp_nav_menu(array('items_wrap'=> '%3$s', 'walker' => new Nav_Social_Walker(), 'container'=>false, 'menu_class' => '', 'theme_location'=>'social', 'fallback_cb'=>false ));

                //cleanernav('social'); ?>
            </nav>
        </section>
        <section class="site-footer-meta">
            <nav class="footer-navigation" role="navigation">
                <?php cleanernav('footer'); ?>
            </nav>
            <div class="credits">
                <span class="site-copyright">Copyright <?php echo date("Y"); ?> <?php echo get_bloginfo('name'); ?>. All rights reserved.</span> // 
                <span class="site-credit"><?php printf( 'Built with <i class="far fa-heart"></i> by %2$s.', 'Wanderoak', '<a href="http://wanderoak.co/" rel="designer">Wanderoak</a>' ); ?></span>
            </div>
        </section>
	</footer><!-- #colophon -->
</div><!-- #page -->
<script>
    
    function openNav() {
        document.getElementById("site-navigation").style.right = "0px";
        document.getElementById("site-navigation").className = "main-navigation open";
    }

    function closeNav() {
        document.getElementById("site-navigation").style.right = "-100%";
        document.getElementById("site-navigation").className = "main-navigation";
    }
</script>
<script>
jQuery(".slide-header").hover(function () {
    jQuery(".slide-content").slideDown("slow");
}, function(){
    jQuery(".slide-content").slideUp("slow");
});
</script>

<script>
    var PI = { 
        onReady: function() {
            jQuery('.mobile-nav').click( PI.mobnav );  
        },
        
        mobnav: function(event) {
            jQuery('#mobile-site-navigation').slideToggle(500);
        },
        
        submenu: function(event) {
            jQuery('.main-navigation li.menu-item-has-children').hover( PI.showsubmenu );
        },
        
        showsubmenu: function(event) {
            jQuery(this).children('.sub-menu').slideToggle(200);
        },    
        specialquestion: function() {
            jQuery('.wpProQuiz_questionList[data-type="multiple"] input').change(PI.specialaddclass);
        },
        specialaddclass: function(event) {

                jQuery(this).parent().parent().toggleClass('checked');
                console.log(this);


        }
        
    };
    
    jQuery(document).ready(PI.specialquestion);
    jQuery(document).ready(PI.submenu);
    
</script> 

<script>
  (function(d) {
    var config = {
      kitId: 'yix8swy',
      scriptTimeout: 3000,
      async: true
    },
    h=d.documentElement,t=setTimeout(function(){h.className=h.className.replace(/\bwf-loading\b/g,"")+" wf-inactive";},config.scriptTimeout),tk=d.createElement("script"),f=false,s=d.getElementsByTagName("script")[0],a;h.className+=" wf-loading";tk.src='https://use.typekit.net/'+config.kitId+'.js';tk.async=true;tk.onload=tk.onreadystatechange=function(){a=this.readyState;if(f||a&&a!="complete"&&a!="loaded")return;f=true;clearTimeout(t);try{Typekit.load(config)}catch(e){}};s.parentNode.insertBefore(tk,s)
  })(document);
</script>

<?php wp_footer(); ?>

</body>
</html>
