<?php
/**
 * WordPress backend modifications by Wanderoak
 *
 * This is functionality added by Aurooba Ahmed
 *
 * @package ArabicDaily_Theme
 */


/**
 * Add Visual Blocks external plugin to TinyMCE
 */

function add_mceplugins() {
     $plugin = array('visualblocks');
     $plugins_array = array();

     foreach ($plugin as $plugin ) {
          $plugins_array[ $plugin ] = get_stylesheet_directory_uri() . '/js/'. $plugin .'/plugin.js';
     }
     return $plugins_array;
}
add_filter('mce_external_plugins', 'add_mceplugins');

function register_visualblocks($in) {
    $in['visualblocks_default_state'] = 'true';
    return $in;
}
add_filter('tiny_mce_before_init', 'register_visualblocks'); 

/**
 * Add Wanderoak credit to footer in backend
 */

function remove_footer_admin () {

echo 'Fueled by <a href="http://www.wordpress.org" target="_blank">WordPress</a> | Designed & Developed by <a href="http://wanderoak.co" target="_blank">Wanderoak</a>';

}

add_filter('admin_footer_text', 'remove_footer_admin');

/**
* Add Support Widget
**/ 

function support_add_dashboard_widgets() {

  wp_add_dashboard_widget('wp_dashboard_widget', 'Support', 'support_info');
    
// Globalize the metaboxes array, this holds all the widgets for wp-admin
 
 	global $wp_meta_boxes;
 	
 	// Get the regular dashboard widgets array 
 	// (which has our new widget already but at the end)
 
 	$normal_dashboard = $wp_meta_boxes['dashboard']['normal']['core'];
 	
 	// Backup and delete our new dashboard widget from the end of the array
 
 	$example_widget_backup = array( 'wp_dashboard_widget' => $normal_dashboard['wp_dashboard_widget'] );
 	unset( $normal_dashboard['wp_dashboard_widget'] );
 
 	// Merge the two arrays together so our widget is at the beginning
 
 	$sorted_dashboard = array_merge( $example_widget_backup, $normal_dashboard );
 
 	// Save the sorted array back into the original metaboxes 
 
 	$wp_meta_boxes['dashboard']['normal']['core'] = $sorted_dashboard;

}

add_action('wp_dashboard_setup', 'support_add_dashboard_widgets' );

function support_info() { ?>

<div class="custom-welcome-panel-content">
	<h3 style="padding-left: 0; font-size: 16px;">Welcome to Your Dashboard!</h3>
	<p><?php _e( 'This dashboard is powered by Wordpress and customized to fit your needs by Wanderoak' ); ?></p>
	<div class="welcome-panel-column-container" style="overflow: hidden;">
	<div class="welcome-panel-column" style="float: left; width: 40%; margin-right: 2em;">
		<h4><?php _e( 'Next Steps' ); ?></h4>
		<ul>
		<?php if ( 'page' == get_option( 'show_on_front' ) && ! get_option( 'page_for_posts' ) ) : ?>
			<li><?php printf( '<a href="%s" class="welcome-icon welcome-edit-page">' . __( 'Edit your front page' ) . '</a>', get_edit_post_link( get_option( 'page_on_front' ) ) ); ?></li>
			<li><?php printf( '<a href="%s" class="welcome-icon welcome-add-page">' . __( 'Add additional pages' ) . '</a>', admin_url( 'post-new.php?post_type=page' ) ); ?></li>
		<?php elseif ( 'page' == get_option( 'show_on_front' ) ) : ?>
			<li><?php printf( '<a href="%s" class="welcome-icon welcome-write-blog">' . __( 'Create an article' ) . '</a>', admin_url( 'post-new.php' ) ); ?></li>
		<?php else : ?>
			<li><?php printf( '<a href="%s" class="welcome-icon welcome-write-blog">' . __( 'Write your first article' ) . '</a>', admin_url( 'post-new.php' ) ); ?></li>
		<?php endif; ?>
			<li><?php printf( '<a href="%s" class="welcome-icon welcome-view-site">' . __( 'View your site' ) . '</a>', home_url( '/' ) ); ?></li>
		</ul>
			<div>
		<h4><?php _e( "Questions?" ); ?></h4>
		<a class="button button-primary button-hero load-customize hide-if-no-customize" href="mailto:hey@wanderoak.co"><?php _e( 'Email Us!' ); ?></a>
<!--			<p class="hide-if-no-customize"><?php printf( __( 'or, <a href="%s">edit your site settings</a>' ), admin_url( 'options-general.php' ) ); ?></p>-->
	</div>
	</div>
	<div class="welcome-panel-column welcome-panel-last">
		<h4><?php _e( 'More Actions' ); ?></h4>
		<ul>
			<li><?php printf( '<div class="welcome-icon welcome-widgets-menus">' . __( 'Manage your <a href="%1$s">navigation menu</a>' ) . '</div>', admin_url( 'nav-menus.php' ) ); ?></li>
			<li><?php printf( '<a href="%s" class="welcome-icon welcome-comments">' . __( 'Turn comments on or off' ) . '</a>', admin_url( 'options-discussion.php' ) ); ?></li>
			<!--<li><php printf( '<a href="%s" class="welcome-icon welcome-learn-more">' . __( 'Learn more about getting started' ) . '</a>', __( 'http://localhost/polishedarrow/wp-admin/index.php?page=wo-walkthrough' ) ); ?></li>-->
		</ul>
	</div>
       <br />
        <h4><?php _e( "Confused?" ); ?></h4>
            <a class="button button-primary button-hero load-customize hide-if-no-customize" href="https://cseb-scbe.org/wpmarine/wp-admin/index.php?page=wo-walkthrough"><?php _e( 'Watch the tutorial!' ); ?></a>
    <!--			<p class="hide-if-no-customize"><?php printf( __( 'or, <a href="%s">edit your site settings</a>' ), admin_url( 'options-general.php' ) ); ?></p>-->
	</div>
</div>
 
  
<?php }

include_once('acf-smart-button/acf-smart-button.php');

function block_title( $title, $field, $layout, $i ) {
	if (!get_sub_field('heading')) {
        return $title;
    }
	// remove layout title from text
	// load text sub field
	if( $text = get_sub_field('heading') ) {
		
		$title .= ': ' . $text;
		
	} elseif ( $text = get_sub_field('button')) {
        $title .= ': ' . $text['text'];
    }
	
	// return
	return $title;
	
}

// name
add_filter('acf/fields/flexible_content/layout_title', 'block_title', 10, 4);

add_action( 'acf/input/admin_enqueue_scripts', function() {
  wp_enqueue_script( 'acf-custom-colors', get_template_directory_uri() . '/js/aw-colors.js', 'acf-input', '1.0', true );
});

function my_acf_collor_palette_css() {
    ?>
    <style>
        .acf-color_picker .iris-picker.iris-border,
        .wp-picker-container .iris-picker {
            width: 200px !important;
            height: 17px !important;
        }
        .acf-color_picker .wp-picker-input-wrap,
        .acf-color_picker .iris-picker .iris-slider,
        .acf-color_picker .iris-picker .iris-square{
            display:none !important;
        }
        .acf-color_picker .iris-picker .iris-palette,
        .iris-picker .iris-square, 
        .iris-picker .iris-slider, 
        .iris-picker .iris-square-inner, 
        .iris-picker .iris-palette {
            box-shadow: 0 0 0 !important;
        }
        .acf-field-5a20600997830 .acf-editor-wrap iframe {
            min-height: 100px !important;
        }

    </style>
    <?php
}

//add_action('acf/input/admin_head', 'my_acf_collor_palette_css');

function acf_load_color_field_choices( $field ) {
    
    // reset choices
    $field['choices'] = array();
    
    
    // get the textarea value from options page without any formatting
    $choices = get_field('my_select_values', 'option', false);

    
    // explode the value so that each line is a new array piece
    $choices = explode("\n", $choices);

    
    // remove any unwanted white space
    $choices = array_map('trim', $choices);

    
    // loop through array and add to field 'choices'
    if( is_array($choices) ) {
        
        foreach( $choices as $choice ) {
            
            $field['choices'][ $choice ] = $choice;
            
        }
        
    }
    

    // return the field
    return $field;
    
}

//add_filter('acf/load_field/name=color', 'acf_load_color_field_choices');


add_action( 'contextual_help', 'wptuts_screen_help', 10, 3 );
function wptuts_screen_help( $contextual_help, $screen_id, $screen ) {
 
    // The add_help_tab function for screen was introduced in WordPress 3.3.
    if ( ! method_exists( $screen, 'add_help_tab' ) )
        return $contextual_help;
 
    global $hook_suffix;
 
    // List screen properties
    $variables = '<ul style="width:50%;float:left;"> <strong>Screen variables </strong>'
        . sprintf( '<li> Screen id : %s</li>', $screen_id )
        . sprintf( '<li> Screen base : %s</li>', $screen->base )
        . sprintf( '<li>Parent base : %s</li>', $screen->parent_base )
        . sprintf( '<li> Parent file : %s</li>', $screen->parent_file )
        . sprintf( '<li> Hook suffix : %s</li>', $hook_suffix )
        . '</ul>';
 
    // Append global $hook_suffix to the hook stems
    $hooks = array(
        "load-$hook_suffix",
        "admin_print_styles-$hook_suffix",
        "admin_print_scripts-$hook_suffix",
        "admin_head-$hook_suffix",
        "admin_footer-$hook_suffix"
    );
 
    // If add_meta_boxes or add_meta_boxes_{screen_id} is used, list these too
    if ( did_action( 'add_meta_boxes_' . $screen_id ) )
        $hooks[] = 'add_meta_boxes_' . $screen_id;
 
    if ( did_action( 'add_meta_boxes' ) )
        $hooks[] = 'add_meta_boxes';
 
    // Get List HTML for the hooks
    $hooks = '<ul style="width:50%;float:left;"> <strong>Hooks </strong> <li>' . implode( '</li><li>', $hooks ) . '</li></ul>';
 
    // Combine $variables list with $hooks list.
    $help_content = $variables . $hooks;
 
    // Add help panel
    $screen->add_help_tab( array(
        'id'      => 'wptuts-screen-help',
        'title'   => 'Screen Information',
        'content' => $help_content,
    ));
 
    return $contextual_help;
}

function wpdocs_enqueue_custom_admin_style() {
    wp_enqueue_script( 'ad-fontawesome-loader', get_template_directory_uri() . '/js/fontawesome.min.js#asyncload', 'jquery', '', true );
    wp_enqueue_script( 'ad-fontawesome-light', get_template_directory_uri() . '/js/light.min.js#asyncload', 'jquery', '', true );
    wp_enqueue_script( 'ad-fontawesome-regular', get_template_directory_uri() . '/js/regular.min.js#asyncload', 'jquery', '', true );
    wp_enqueue_script( 'ad-fontawesome-solid', get_template_directory_uri() . '/js/solid.min.js#asyncload', 'jquery', '', true );
}
add_action( 'admin_enqueue_scripts', 'wpdocs_enqueue_custom_admin_style' );

function my_remove_meta_boxes() {
    remove_meta_box( 'ld_lesson_categorydiv', 'sfwd-lessons', 'side' );
    remove_meta_box( 'categorydiv', 'sfwd-quiz', 'side' );
    remove_meta_box( 'tagsdiv-post_tag', 'sfwd-quiz', 'side' );
    register_taxonomy_for_object_type('ld_lesson_category', 'sfwd_quiz');
}
add_action( 'admin_menu', 'my_remove_meta_boxes' );

add_action('init','add_categories_to_cpt');
function add_categories_to_cpt(){
    register_taxonomy_for_object_type('ld_lesson_category', 'sfwd_quiz');
}

function ad_login_redirect( $redirect_to, $request, $user ) {
    //is there a user to check?
    if (isset($user->roles) && is_array($user->roles)) {
        //check for subscribers
        if (in_array('subscriber', $user->roles) || in_array('student', $user->roles)) {
            // redirect them to another URL, in this case, the homepage 
            $redirect_to = get_field('student_dashboard', 'option');
        }
    }

    return $redirect_to;
}

add_filter( 'login_redirect', 'ad_login_redirect', 10, 3 );



add_filter('woocommerce_login_redirect', 'wc_login_redirect');
 
function wc_login_redirect( $redirect_to ) {
     $redirect_to = get_field('student_dashboard', 'option');
     return $redirect_to;
}

add_action('after_setup_theme', 'ad_disable_admin_bar');

function ad_disable_admin_bar() {
    
    $user = wp_get_current_user();
    
    if (in_array('subscriber', $user->roles) || in_array('student', $user->roles)) {
        add_filter('show_admin_bar', '__return_false');
    }
}