<section class="builder tiles">
    <?php
    $styling = get_sub_field('styling');
    // check if the repeater field has rows of data
    if( have_rows('tile') ):

        // loop through the rows of data
        while ( have_rows('tile') ) : the_row();
            $icon = get_sub_field('icon');
            $heading = get_sub_field('heading');
            $content = get_sub_field('text');
            $interaction = get_sub_field('interaction_type');
    
        ?>
            <article class="tile <?php echo $styling['background_colour']; ?>">
                <header>
                  <?php if ($icon) : ?>
                       <figure>
                           <img src="<?php echo $icon['url']; ?>" />
                       </figure>
                   <?php endif; ?>
                    <h2><?php echo $heading; ?></h2>

                </header>
                <div class="tile-content">
                    <?php echo $content; ?>
                </div>
                <?php if ($interaction == 'button') : ?>
                    <?php
                    $button = get_sub_field('button');
                    $button_url = $button['url'];
                    $findme   = 'product';
                    $pos = strpos($button_url, $findme);
                    $product_slug = substr($button_url, $pos+7);
                    $product_obj = get_page_by_path( $product_slug, OBJECT, 'product' );
                    if ($product_obj) :
                        $button_url = home_url() . '/?add-to-cart=' . $product_obj->ID;
                    endif;
                    ?>
                    <div class="button-wrapper">
                        <a class="button <?php if ($styling['background_colour'] == 'transparent' || $styling['background_colour'] == 'beige'): echo 'button-' . substr($styling['button_colour'], 1); endif; ?>" href="<?php echo $button_url; ?>" target="<?php echo $button['target']; ?>"><?php echo $button['text']; ?></a>
                    </div>
                <?php elseif ($interaction == 'optin') : ?>
                    <?php
                    $optin_id = get_sub_field('email_optin');
                    $colour = $styling['button_colour'];
                    $list = get_field('choose_list', $optin_id);
                    $groups = get_field('interest_group', $optin_id);
                    $current_title = get_the_title();
                    ?>
                    <div class="ad-optin">
                        <form method="post" action="<?php echo admin_url('admin-ajax.php'); ?>" data-womc>
                            <div class="womc-fields" data-womc-fields>
                                <input class="<?php echo wo_colour_class($colour); ?>" type="email" name="email" placeholder="email address" aria-required="true" aria-invalid="false">
                                <input class="<?php echo wo_colour_class($colour); ?>" type="submit" value="Sign Up" name="subscribe">
                                <input type="hidden" name="list_id" value="<?php echo $list; ?>">
                                <input type="hidden" name="signup" value="<?php echo $current_title; ?>">
                                <?php 
                                if ($groups): ?>
                                    <input type="hidden" name="group_id" value="<?php echo $groups; ?>">
                                <?php endif; ?>
                                <input type="hidden" name="action" value="womc_subscribe">
                                <?php wp_nonce_field( 'womc_subscribe_nonce', 'post_mc_sub' ); ?>
                            </div>
                            <div class="womc-success-message hide" data-womc-success>
                               <?php
                                    the_field('default_optin_success_message', 'option'); 
                                ?>
                            </div>
                            <div class="womc-already-sub-message hide" data-womc-error>
                                <p>It looks like you're already subscribed. Yay!</p>
                            </div>
                        </form>

                    </div>
                <?php endif; ?>
            </article>

        <?php 
        endwhile;

    else :

        // no rows found

    endif;

    ?>
</section>
