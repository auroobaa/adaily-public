<?php 
    $heading = get_sub_field('heading');
    $content = get_sub_field('text');
    $styling = get_sub_field('styling');    
?>

<section class="builder long-form" style="background-color: <?php echo $styling; ?>">
  <h2><?php echo $heading; ?></h2>
   <div class="content-wrapper">
        <?php echo $content; ?>
    </div>
</section>
