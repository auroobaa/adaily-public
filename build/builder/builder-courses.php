<?php 

$heading = get_sub_field('heading');
$course_selection = get_sub_field('course_selection');
if ($course_selection == 'track') :
    $course = get_sub_field('choose_track');
elseif ($course_selection == 'custom')  :
    $course = 'custom';
endif;

?>


<section class="builder course-list">
   <h2 class="title"><?php echo $heading; ?></h2>
   <?php if ($course == 'custom') : 

        // check if the repeater field has rows of data
        if( have_rows('choose_courses') ):
            // loop through the rows of data
            while ( have_rows('choose_courses') ) : the_row();

                $course = get_sub_field('course');
                $course_title = get_the_title($course);
                $course_subtitle = get_field('course_subtitle', $course);
                $course_descriptor = get_field('course_descriptor', $course);
                $content_course = get_field('course_blurb', $course);
//                $content = $content_course->post_content;
//                $content = apply_filters('the_content', $content);
//                $content = str_replace(']]>', ']]&gt;', $content);
                $course_link = get_permalink($course);
                
                ?>
                <a href="<?php echo $course_link; ?>">
                <article class="course-list-course">
                    <header>
                       <div class="content-wrapper">
                            <span><?php echo $course_subtitle; ?></span>
                            <h2><?php echo $course_title; ?></h2>
                            <p><?php echo $course_descriptor; ?></p>
                        </div>
                    </header>
                    <div class="course-list-description">
                       <div class="content-clip"><div class="content-wrapper">
                        <?php echo $content_course; ?>
                        </div></div>
                    </div>
                </article>
                </a>
                
            <?php 
            endwhile;

        else :

            // no rows found

        endif;
    else :
        $courses = get_field('courses', $course);
        $zindex = 700;
        foreach ($courses as $scourse) {
            
            $course_title = get_the_title($scourse);
            $course_subtitle = get_field('course_subtitle', $scourse);
            $course_descriptor = get_field('course_descriptor', $scourse);
            $content_course = get_field('course_blurb', $scourse);
//            $content = $content_course->post_content;
//            $content = apply_filters('the_content', $content);
//            $content = str_replace(']]>', ']]&gt;', $content);
            $course_link = get_permalink($scourse);
                
            ?>
            <a href="<?php echo $course_link; ?>">
            <article class="course-list-course">
                <header>
                   <div class="content-wrapper">
                        <span><?php echo $course_subtitle; ?></span>
                        <h2><?php echo $course_title; ?></h2>
                        <p><?php echo $course_descriptor; ?></p>
                    </div>
                </header>
                <div class="course-list-description">
                   <div class="content-clip"><div class="content-wrapper">
                    <?php echo $content_course; ?>
                    </div></div>
                </div>
                </article>
            </a>
            
        <?php }
    endif;
    ?>
</section>