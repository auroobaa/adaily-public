<?php 

$which = get_sub_field('which_blog_posts');
$quantity = get_sub_field('quantity');
$heading = get_sub_field('heading');

if ($which == 'category') :
    $category = get_sub_field('category');
    $args = array( 'numberposts' => $quantity, 'category' => $category );
    $posts = wp_get_recent_posts( $args );

elseif ($which == 'recent') :
    $args = array( 'numberposts' => $quantity );
    $posts = wp_get_recent_posts( $args );

elseif ($which == 'custom') :
    $posts = get_sub_field('custom');

endif;

?>


<section class="builder blog-posts">
   <?php if ($heading) : ?>
       <h2><?php echo $heading; ?></h2>
   <?php endif; ?>
   <div class="component-wrapper">
    <?php 
    foreach ($posts as $post):
        $category = get_the_category($post['ID']);
        $category_colour = wo_colour_class(get_field('color', $category[0]));
        if (!get_the_post_thumbnail($post['ID'])): 
            $classes = 'no-featured-image';
        else :
            $classes = '';
        endif;
    ?>
    
        <a class="blog-post-link <?php echo $classes; ?>" href="<?php echo get_permalink($post['ID']); ?>">
            <article>
               <header>
                  <div class="content-wrapper">
                   <h4><?php echo $post['post_title']; ?></h4>
                   <h5 class="<?php echo $category_colour; ?>"><?php echo $category[0]->name; ?></h5>
                   </div>
               </header>
               <?php if (get_the_post_thumbnail($post['ID'])): ?>
               <div class="image-wrapper">
                  <div class="image">
                       <?php echo get_the_post_thumbnail($post['ID']); ?>
                   </div>
               </div>
               <?php endif; ?>
            </article>
        </a>
        
    <?php endforeach; wp_reset_query();
    ?>
    </div>
</section>