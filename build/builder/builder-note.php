<?php 

$note = get_sub_field('note');
$button = get_sub_field('button');
$button_details = $button['button_details'];
$button_colour = $button['button_colour'];
$buttonc = substr($button_colour, 1);

$button_url = $button_details['url'];
$findme = 'product';
$pos = strpos($button_url, $findme);
$product_slug = substr($button_url, $pos+7);
$product_obj = get_page_by_path( $product_slug, OBJECT, 'product' );
if ($product_obj) :
    $button_url = home_url() . '/?add-to-cart=' . $product_obj->ID;
endif;


?>


<section class="builder note">
    <p><?php echo $note; ?></p>
    <?php if ( $button_details ) : ?> 
    <style>
        .button-wrapper a {
            background-color: <?php echo $button_colour; ?>;
            border: 1px solid <?php echo $button_colour; ?>;
        }
        .button-wrapper a:hover {
            background-color: transparent;
            border: 1px solid <?php echo $button_colour; ?>;
            color: <?php echo $button_colour; ?>
        }
    </style>
    <div class="button-wrapper">
        <a href="<?php echo $button_url; ?>" target="<?php echo $button_details['target']; ?>" class="button button-<?php echo $buttonc; ?>"><?php echo $button_details['text']; ?></a>
    </div>
    <?php endif; ?>
</section>