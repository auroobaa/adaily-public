<?php

$media_type = get_sub_field('media_type');

if ($media_type == 'image') :
    $media = get_sub_field('image'); ?>
    <section class="builder full-width-image">
        <figure>
            <img src="<?php echo $media['url']; ?>" />
        </figure>
    </section>
<?php 
elseif ($media_type == 'video') :
    $media = get_sub_field('video'); ?>
    <section class="builder full-width-video">
            <?php echo $media; ?>
    </section>
<?php
endif;
        
?>