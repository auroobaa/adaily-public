jQuery(document).ready(function(){ 
    jQuery( ".mark-complete-message" ).click(function() {
        jQuery( this ).next( ".ad-confirm-mark-complete-wrapper" ).animate({width: 'toggle'});
        jQuery( this ).hide();
    });
    jQuery( ".no-complete" ).click(function() {
        var wrapper = jQuery( this ).parent().parent();
        jQuery( wrapper ).hide();
        jQuery( wrapper ).prev().fadeIn();
//        jQuery( this ).hide();
    });
});