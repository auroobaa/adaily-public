jQuery(document).ready(function ($) {
		// make sure acf is loaded, it should be, but just in case
		if (typeof acf == 'undefined') { return; }
		
		// extend the acf.ajax object
		// you should probably rename this var
        // PAGE BUILDER
		var wswginterestcacf = acf.ajax.extend({
			events: {
				// this data-key must match the field key for the list field on the post page where
				// you want to dynamically load interests when the list is changed
				'change [data-key="field_5b16d21f1b1f7"] select': '_list_change',
				// this entry is to cause the interest field to be updated when the page is loaded
//				'ready [data-key="field_5a5eb8c87a15e"] select': '_list_change',
			},
			
			// this is our function that will perform the
			// ajax request when the list value is changed
			_list_change: function(e){
				
				// clear the interest field options
				// the data-key is the field key of the interest on post
				var $select = $('[data-key="field_5b16d22e1b1f8"] select');
				$select.empty();
				
				// get the list selection
				var $value = e.$el.val();
				// a lot of the following code is copied directly 
				// from ACF and modified for our purpose
				
				// I assume this tests to see if there is already a request
				// for this and cancels it if there is
				if( this.list_request) {
					this.list_request.abort();
				}
				
				// I don't know exactly what it does
				// acf does it so I copied it
				var self = this,
						data = this.o;
                
						
				// set the ajax action that's set up in php
				data.action = 'load_mc_interest';
				// set the list value to be submitted
				data.list = $value;
				
				// this is another bit I'm not sure about
				// copied from ACF
				data.exists = [];
				
				// this is the request format and it's copied from ACF, but it's a standard json request really
				this.list_request = $.ajax({
					url:		acf.get('ajaxurl'),
					data:		acf.prepare_for_ajax(data),
					type:		'post',
					dataType:	'json',
					async: true,
					success: function(json){

						// the field key that we want to update
						var $select = $('[data-key="field_5b16d22e1b1f8"] select');
						
						// add options to the interest field
						for (i=0; i<json.length; i++) {
							var $item = '<option value="'+json[i]['value']+'">'+json[i]['label']+'</option>';
							$select.append($item);
						}
					}
				});
			},
		});   
    
		var wswggroupcacf = acf.ajax.extend({
			events: {
				// this data-key must match the field key for the list field on the post page where
				// you want to dynamically load groups when the interest is changed
				'change [data-key="field_5b16d22e1b1f8"] select': '_interests_change',
				// this entry is to cause the group field to be updated when the page is loaded
//				'ready [data-key="field_5a5eb8d17a15f"] select': '_interests_change',
			},
			
			// this is our function that will perform the
			// ajax request when the interest value is changed
			_interests_change: function(e){
				
				// clear the group field options
				// the data-key is the field key of the groups on post
				var $select = $('[data-key="field_5b16d24a1b1f9"] select');
//                var $saved = $select.val();
//                console.log("saved " + $saved);
//                console.log("saved " + $saved);
				$select.empty();
				
                // in order to grab groups, we also need the list field, which we grab
                // from the this field select
                var $list_field = $('[data-key="field_5b16d21f1b1f7"] select');
                
				// get the list selection from the field select above
                var $list = $list_field.val();
                
                // get the interest that's been selected
				var $interests = e.$el.val();
				// a lot of the following code is copied directly 
				// from ACF and modified for our purpose
				
				// I assume this tests to see if there is already a request
				// for this and cancels it if there is
				if( this.interests_request) {
					this.interests_request.abort();
				}
				
				// I don't know exactly what it does
				// acf does it so I copied it
				var self = this,
						data = this.o;
                
				// set the ajax action that's set up in php
				data.action = 'load_mc_groups';
				// set the list value to be submitted
				data.list = $list;
                data.interests = $interests;
				
				// this is another bit I'm not sure about
				// copied from ACF
				data.exists = [];
				
				// this is the request format and it's copied from ACF, but it's a standard json request really
				this.interests_request = $.ajax({
					url:		acf.get('ajaxurl'),
					data:		acf.prepare_for_ajax(data),
					type:		'post',
					dataType:	'json',
					async: true,
					success: function(json){

						// the field key that we want to update
						var $select = $('[data-key="field_5b16d24a1b1f9"] select');
						
						// add options to the groups field
						for (i=0; i<json.length; i++) {
							var $item = '<option value="'+json[i]['value']+'">'+json[i]['label']+'</option>';
							$select.append($item);
						}
							
							$select.append($item);
						}
					
				});
			},
		});
		
		// trigger the ready action on page load for the _list_change
//		$('[data-key="field_5a5eb8c87a15e"] select').trigger('ready');
    
//        $('[data-key="field_5a5eb8d17a15f"] select').trigger('ready');
	});