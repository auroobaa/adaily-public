<?php

add_filter( 'woocommerce_cart_item_thumbnail', '__return_false' );
//
//class learndash_woocommerce {
//    
//    public function __construct() {
//
//        remove_action( 'woocommerce_order_status_completed', array( $this, 'send_receipt' ), 10, 1 );
//        remove_action( 'woocommerce_order_status_processing', array( $this, 'send_receipt' ), 10, 1 );
//        add_action( 'woocommerce_order_status_completed', array( $this, 'wo_send_receipt' ), 10, 1 );
//        add_action( 'woocommerce_order_status_processing', array( $this, 'wo_send_receipt' ), 10, 1 );
//
//    }
//    	public function wo_send_receipt( $order_id ) {
//
//		$order = new WC_Order( $order_id );
//		if ( ($order) && (( $order->has_status( 'completed' ) ) || ( $order->has_status ( 'processing' ) ) ) ) {
//			$products = $order->get_items();
//
//			foreach($products as $product){
//				$courses_id = get_post_meta( $product['product_id'], '_related_course', true );
//
//				if ( $courses_id && is_array( $courses_id ) ) {
//					foreach ( $courses_id as $cid ) {
//						$this->add_course_access( $cid, $order->customer_user );
//
//						// if WooCommerce subscription plugin enabled
//						if ( class_exists( 'WC_Subscriptions' ) ) {
//							// If it's a subscription...
//							if ( WC_Subscriptions_Order::order_contains_subscription($order) || WC_Subscriptions_Renewal_Order::is_renewal( $order ) ) {
//								error_log("Subscription (may be renewal) detected");
//								if ( $sub_key = WC_Subscriptions_Manager::get_subscription_key($order_id, $product['product_id'] ) ) {
//									error_log("Subscription key: " . $sub_key );
//									$subscription_r = WC_Subscriptions_Manager::get_subscription( $sub_key );
//									$start_date = $subscription_r['start_date'];
//									error_log( "Start Date:" . $start_date );
//									update_user_meta( $order->customer_user, "course_".$cid."_access_from", strtotime( $start_date ) );
//								}
//							}
//						}
//					}
//				}
//			}
//		}
//	}
//}

//add_action( 'woocommerce_init', 'remove_wcpgsk_email_order_table' );
function remove_wcpgsk_email_order_table() {

    global $learndash_woocommerce;
    remove_action( 'woocommerce_email_after_order_table', array( $wcpgsk, 'wcpgsk_email_after_order_table' ) );

}

function wo_send_receipt( $order_id ) {

    $order = new WC_Order( $order_id );
    if ( ($order) && (( $order->has_status( 'completed' ) ) || ( $order->has_status ( 'processing' ) ) ) ) {
        $products = $order->get_items();

        foreach($products as $product){
            $courses_id = get_post_meta( $product['product_id'], '_related_course', true );

            if ( $courses_id && is_array( $courses_id ) ) {
                foreach ( $courses_id as $cid ) {
                    $this->add_course_access( $cid, $order->customer_user );

                    // if WooCommerce subscription plugin enabled
                    if ( class_exists( 'WC_Subscriptions' ) ) {
                        // If it's a subscription...
                        if ( WC_Subscriptions_Order::order_contains_subscription($order) || WC_Subscriptions_Renewal_Order::is_renewal( $order ) ) {
                            error_log("Subscription (may be renewal) detected");
                            if ( $sub_key = WC_Subscriptions_Manager::get_subscription_key($order_id, $product['product_id'] ) ) {
                                error_log("Subscription key: " . $sub_key );
                                $subscription_r = WC_Subscriptions_Manager::get_subscription( $sub_key );
                                $start_date = $subscription_r['start_date'];
                                error_log( "Start Date:" . $start_date );
                                update_user_meta( $order->customer_user, "course_".$cid."_access_from", strtotime( $start_date ) );
                            }
                        }
                    }
                }
            }
        }
    }
}

//function wo_woocommerce_login_redirect( $redirect, $user ) {
//    
////    if (isset($user->roles) && is_array($user->roles)) {
//        //check for subscribers
////        if ( in_array('subscriber', $user->roles) || in_array('student', $user->roles) || in_array('customer', $user->roles) ) {
//            // redirect them to another URL, in this case, the homepage 
//            $redirect = get_field('student_dashboard', 'option');
////        }
////    }
//
//    return $redirect;
////return 'hello';    
//}
 
//add_filter( 'woocommerce_login_redirect', 'wo_woocommerce_login_redirect', 10, 2 );


if ( class_exists( 'WooCommerce' ) ) {
  add_filter( 'wp_nav_menu_top-right_items', 'wo_append_cart_icon', 10, 2 );
} else {
  // you don't appear to have WooCommerce activated
}

function wo_append_cart_icon( $items, $args ) {

	$cart_item_count = WC()->cart->get_cart_contents_count();
	$cart_count_span = '';
	if ( $cart_item_count ) {
		$cart_count_span = '<span class="count">'.$cart_item_count.'</span>';

        $cart_link = '<li class="cart menu-item menu-item-type-post_type menu-item-object-page"><a href="' . get_permalink( wc_get_page_id( 'cart' ) ) . '">Cart ('.$cart_count_span.')</a></li>';
        // Add the cart link to the end of the menu.
        $items = $cart_link . $items;
    }
	return $items;
}