<?php 
    $qa = get_sub_field('q&a');
    $styling = get_sub_field('styling');
?>


<section class="builder faq" style="text-align: <?php echo $styling; ?>">
    <?php

    // check if the repeater field has rows of data
    if( have_rows('q&a') ):

        // loop through the rows of data
        while ( have_rows('q&a') ) : the_row();

            $question = get_sub_field('question');
            $answer = get_sub_field('answer');
            ?>
            <article class="qa">
                <dt><?php echo $question; ?></dt>
                <dd><?php echo $answer; ?></dd>
            </article>

        <?php 
        endwhile;

    else :

        // no rows found

    endif;

    ?>
</section>
<script type="text/javascript">
    jQuery(document).ready(function() {
        jQuery('.faq dt').click(function() {
            jQuery(this).next('dd').slideToggle(500);

        });
    }); // end ready
</script>