<?php 

$mediagroup = get_sub_field('media');
$media_type = $mediagroup['media_type'];
if ($media_type == 'image') :
    $media = $mediagroup['image'];
elseif ($media_type == 'video') :
    $media = $mediagroup['video'];
endif;

$heading = get_sub_field('heading');
$text = get_sub_field('text');

$hide = get_sub_field('hide_option');
?>

<section class="multimedia">
        <span class="hidden-reveal"><i class="fa fa-angle-double-right"></i>View <?php echo $heading; ?></span>
    <div class="builder total-wrapper">
        <div class="content-wrapper">  
        <?php if ($hide) : ?>
             <span class="hide"><i class="fa fa-times"></i> HIDE</span>
         <?php endif; ?>
          <div class="content">
               <h2><?php echo $heading; ?></h2>
               <?php echo $text; ?>
           </div>
        </div>
        <div class="media <?php if ($media_type == 'image') : echo 'image'; endif; ?>">
            <?php if ($media_type == 'image') : ?>
                <figure><img src="<?php echo $media['url'];?>" /></figure>
            <?php elseif ($media_type == 'video') : ?>
                <?php echo $media; ?>
            <?php endif; ?>
        </div>
    </div>
</section>
<script>
    var MI = { 
        hideButton: function() {
            jQuery('.hide').click( MI.hideWrapper );  
        },
        
        hideWrapper: function(event) {
            jQuery('.total-wrapper').slideUp( 500 );
            MI.showRevealer();
        },
        
        showRevealer: function(event) {
            jQuery('.hidden-reveal').fadeIn( 500 );
        }
        
    };
    
    var HI = { 
        revealerButton: function() {
            jQuery('.hidden-reveal').click( HI.hideRevealer);  
        },
        
        showWrapper: function(event) {
            jQuery('.total-wrapper').slideDown( 500 );
//            HI.hideRevealer();
        },
        
        hideRevealer: function(event) {
            jQuery('.hidden-reveal').fadeOut( 500 );
            HI.showWrapper();
        }
        
    };
    
    jQuery(document).ready(HI.revealerButton);
    jQuery(document).ready(MI.hideButton);
    
</script> 

<!--
<script type="text/javascript">
    jQuery(document).ready(function() {
        jQuery('.hide').click(function() {
            jQuery('.total-wrapper').slideUp();
            jQuery('.hidden-reveal').slideDown();

        });
        jQuery('.hidden-reveal').click(function() {
            jQuery('.total-wrapper').slideDown();
            jQuery('.hidden-reveal').slideUp();

        });
    }); 
</script>-->
