<?php
    $background = get_sub_field('styling');
    $heading = get_sub_field('heading');
    $text = get_sub_field('text');
    $button = get_sub_field( 'button' );
    $button_label = $button['text'];
    $button_url = $button['url'];
    if ($button_url) :
        $findme = 'product';
        $pos = strpos($button_url, $findme);
        $product_slug = substr($button_url, $pos+7);
        $product_obj = get_page_by_path( $product_slug, OBJECT, 'product' );
    //    echo '<pre>' . var_export($product_obj, true) . '</pre>';
        if ($product_obj) :
            $button_url = home_url() . '/?add-to-cart=' . $product_obj->ID;
        endif;
    endif;
    $button_target = $button['target'];
    $image = get_sub_field('icon');
    $note = get_sub_field('note');
        
?>
<section class="builder cta <?php echo $background['background_colour']; ?>">
    <?php if ($image) : ?>
        <figure>
            <img src="<?php echo $image['url']; ?>">
        </figure>
    <?php endif; ?>
    <?php if ($heading) : ?><h3 class="heading"><?php echo $heading; ?></h3> <?php endif; ?>

    <div class="cta-text">
        <?php echo $text; ?>
    </div>
    
    <?php if ( $button_url ) : ?> 
    <div class="button-wrapper">
        <a class="button ghost-button <?php if ($background['background_colour'] == 'transparent' || $background['background_colour'] == 'beige'): echo 'button-' . substr($background['button_colour'], 1); endif; ?>" href="<?php echo $button_url; ?>" target="<?php echo $button_target; ?>"><?php echo $button_label; ?></a>
    </div>
    <?php endif; ?>
    
    <?php if ( $note ) : ?> 
    <div class="note <?php if ($background['background_colour'] == 'orange' || $background['background_colour'] == 'blue'): echo 'note-white'; endif; ?>">
        <p><?php echo $note; ?></p>
    </div>
    <?php endif; ?>
    
</section>