<?php
$heading = get_sub_field('heading');
$testimonials = get_sub_field('choose_testimonials');
?>

<section class="builder testimonials">
   <?php if ($heading) : ?>
       <h2><?php echo $heading; ?></h2>
   <?php endif; ?>
   <div class="component-wrapper" data-flickity='{ "cellAlign": "center", "groupCells": "1", "pageDots": false, "draggable": false, "autoPlay": true }'>
    <?php
        foreach ($testimonials as $testimonial) {
            if (get_the_post_thumbnail($testimonial)) :
                $pic = get_the_post_thumbnail($testimonial);
            else : 
                $pic = false;
            endif;
            $post_object = get_post($testimonial);
            $quote = $post_object->post_excerpt;
            $content = $post_object->post_content;
            if (!$quote && (strlen($content) > 250)) :
                $quote =  substr($content, 0, strpos($content, ' ', 250)) . '...';
//            else : 
//                $quote = $content;
            endif;
            $name = $post_object->post_title;
            $link = str_replace(' ', '', $name);
       ?>
                <article class="testimonial <?php if (!$pic): echo 'no-pic'; endif; ?>">
                   <?php if ($pic) : ?>
                    <header>
                        <figure>
                            <?php echo $pic; ?>
                        </figure>
                    </header>
                    <?php endif; ?> 
                    <div class="content">
                        <?php echo $quote; ?>
                    </div>
                    <div id="<?php echo $link . '-full-quote';?>"class="full-quote mfp-hide">
                       <?php if ($pic) : ?>
                        <figure>
                            <?php echo $pic; ?>
                        </figure>
                        <?php endif; ?> 
                        <div class="full-quote__content">
                            <?php echo apply_filters( 'the_content', $content ); ?>
                        </div>
                        <cite> <?php echo $name; ?>, <?php echo get_field('subtitle', $testimonial); ?></cite>
                    </div>
                    <footer>
                       <cite>  <?php echo $name; ?>, <?php echo get_field('subtitle', $testimonial); ?></cite>
                    <div class="testimonial-read-more">
                        <a href="#<?php echo $link . '-full-quote';?>" class="<?php echo $link . '-open'; ?>"><span>Read More</span> <i class="fa fa-angle-double-right"></i></a>
                    </div>
                    </footer>
                </article>
                <script>
                    jQuery(document).ready(function() {
                        jQuery('.<?php echo $link . '-open'; ?>').magnificPopup({
                          type:'inline',
                          midClick: true,
//                          overflowY: scroll
                        });
                    });
                </script>
        <?php } ?>
        </div>
</section>
<?php //ad_mc_lists(); ?>