<?php
    $heading = get_sub_field('heading');
    $optin_id = get_sub_field('choose_optin');
    $list = get_field('choose_list', $optin_id);
    $groups = get_field('interest_group', $optin_id);
    $current_title = get_the_title();
    $colour = get_field('colour', $optin_id);
    $note = get_sub_field('note');
?>
   

   
<section class="ad-optin">
   <?php if ($heading) : ?>
       <h2><?php echo $heading; ?></h2>
   <?php endif; ?>
    <form method="post" action="<?php echo admin_url('admin-ajax.php'); ?>" data-womc>
        <div class="womc-fields" data-womc-fields>
            <input class="<?php echo wo_colour_class($colour); ?>" type="email" name="email" placeholder="email address" aria-required="true" aria-invalid="false">
            <input class="<?php echo wo_colour_class($colour); ?>" type="submit" value="Sign Up" name="subscribe">
            <input type="hidden" name="list_id" value="<?php echo $list; ?>">
            <input type="hidden" name="signup" value="<?php echo $current_title; ?>">
            <?php 
            if ($groups): ?>
                <input type="hidden" name="group_id" value="<?php echo $groups; ?>">
            <?php endif; ?>
            <input type="hidden" name="action" value="womc_subscribe">
            <?php wp_nonce_field( 'womc_subscribe_nonce', 'post_mc_sub' ); ?>
        </div>
        <div class="womc-success-message hide" data-womc-success>
           <?php
                the_field('default_optin_success_message', 'option'); 
            ?>
        </div>
        <div class="womc-already-sub-message hide" data-womc-error>
            <p>It looks like you're already subscribed. Yay!</p>
        </div>
    </form>
    <div class="note">
        <p><?php echo $note; ?></p>
    </div>
</section>