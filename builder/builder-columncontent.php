<?php


?>


<section class="builder column-content-wrapper">
   <?php
    if( have_rows('column_content') ):

    // loop through the rows of data
    while ( have_rows('column_content') ) : the_row();
        $mediagroup = get_sub_field('media');
        $media_type = $mediagroup['media_type'];
        if ($media_type == 'image') :
            $media = $mediagroup['image'];
        elseif ($media_type == 'video') :
            $media = $mediagroup['video'];
        endif;

        $contentgroup = get_sub_field('content');
        $heading = $contentgroup['heading'];
        $text = $contentgroup['text'];
        $interaction = $contentgroup['interaction'];
        if ($interaction == 'button') :
            $button = $contentgroup['button'];
            $button_colour = $contentgroup['button_colour'];
            $button_class = substr($button_colour, 1);
            $button_url = $button['url'];
            $findme   = 'product';
            $pos = strpos($button_url, $findme);
            $product_slug = substr($button_url, $pos+7);
            $product_obj = get_page_by_path( $product_slug, OBJECT, 'product' );
            if ($product_obj) :
                $button_url = home_url() . '/?add-to-cart=' . $product_obj->ID;
            endif;
        elseif ($interaction == 'optin') :
            $optin_id = $contentgroup['email_optin'];
        endif;
        $heading_size = $contentgroup['heading_size'];
    
    ?>
       <article class="column-content">
        <?php if ($media_type == 'image') :
            echo '<figure><img src="' . $media['url'] . '"></figure>';
        elseif ($media_type == 'video') :
            echo '<div class="video">' . $media . "</div>";
        endif; ?>
        <div class="content-wrapper">
           <?php if ($heading_size == 'big') : ?>
                <h2 class="h1"><?php echo $heading; ?></h2>
            <?php else : ?>
                <h2><?php echo $heading; ?></h2>
            <?php endif; ?>
            <?php echo $text; ?>
            <?php if ($interaction == 'button') : ?>
                <?php if ($button['url']) : ?>
                <div class="button-wrapper">
                    <a href="<?php echo $button_url; ?>" class="button button-<?php echo $button_class; ?>" target="<?php echo $button['target']; ?>"><?php echo $button['text']; ?></a>
                </div>
                <?php endif; ?>
            <?php elseif ($interaction == 'optin') : ?>
                <?php
                    $colour = get_field('colour', $optin_id);
                    $list = get_field('choose_list', $optin_id);
                    $groups = get_field('interest_group', $optin_id);
                    $current_title = get_the_title();
                ?>
                <div class="ad-optin">
                    <form method="post" action="<?php echo admin_url('admin-ajax.php'); ?>" data-womc>
                        <div class="womc-fields" data-womc-fields>
                            <input class="<?php echo wo_colour_class($colour); ?>" type="email" name="email" placeholder="email address" aria-required="true" aria-invalid="false">
                            <input class="<?php echo wo_colour_class($colour); ?>" type="submit" value="Sign Up" name="subscribe">
                            <input type="hidden" name="list_id" value="<?php echo $list; ?>">
                            <input type="hidden" name="signup" value="<?php echo $current_title; ?>">
                            <?php 
                            if ($groups): ?>
                                <input type="hidden" name="group_id" value="<?php echo $groups; ?>">
                            <?php endif; ?>
                            <input type="hidden" name="action" value="womc_subscribe">
                            <?php wp_nonce_field( 'womc_subscribe_nonce', 'post_mc_sub' ); ?>
                        </div>
                        <div class="womc-success-message hide" data-womc-success>
                           <?php
                                the_field('default_optin_success_message', 'option'); 
                            ?>
                        </div>
                        <div class="womc-already-sub-message hide" data-womc-error>
                            <p>It looks like you're already subscribed. Yay!</p>
                        </div>
                    </form>

                </div>
            <?php endif; ?>
           </div>
        </article>
    <?php
    endwhile;

    else :

        // no rows found

    endif;
    ?>
</section>
