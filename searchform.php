<?php
/**
 * Template for the search form!
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package The_Tempest
 */

?>
   

<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) )?>">
    <label>
        <span class="screen-reader-text">Search for:..</span>
        <input type="search" class="search-field" placeholder="" value="" name="s" title="" />
<!--        <i class="fa fa-search"></i>-->
    </label>
    <input type="submit" class="search-submit" value="search" />
</form>