var gulp = require('gulp'),
    plumber = require('gulp-plumber'),
    sass = require('gulp-ruby-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    newer = require('gulp-newer'),
    imagemin = require('gulp-imagemin'),
    lr = require('tiny-lr'),
    server = lr()
 
    
gulp.task('styles', function(){
    return sass('scss/*.scss', {
      precision: 6,
      stopOnError: false,
      cacheLocation: 'scss/temp',
      style: 'compressed',
      loadPath: 'scss',
    })
        .pipe(plumber())
        .on('error', sass.logError)
	    .pipe(autoprefixer({browsers:['last 2 versions']}))
	    .pipe(gulp.dest(''))
        .pipe(gulp.dest('build'))
});

gulp.task('update PHP', function(){
	return gulp.src(['*.php', '**/*.php', '!build/**/*.php'])
	    .pipe(plumber())
        .pipe(gulp.dest('build'))
});

gulp.task('update JS', function(){
    return gulp.src('js/*.js')
        .pipe(plumber())
        .pipe(gulp.dest('build/js'))
});
 
var imgSrc = 'imgs/originals/*';
var imgDest = 'imgs';
 
gulp.task('images', function() {
  return gulp.src(imgSrc, {base: 'imgs/originals'})
        .pipe(newer(imgDest))
        .pipe(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true }))
        .pipe(gulp.dest(imgDest))
        .pipe(gulp.dest('build/imgs'))
});

gulp.task('screenshot', function() {
    return gulp.src('screenshot.png')
        .pipe(gulp.dest('build'))
});
 

gulp.task('update fontkit', function(){
    return gulp.src('fontkit/*')
        .pipe(plumber())
        .pipe(gulp.dest('build/fontkit'))
});

gulp.task('default', ['styles', 'images', 'update PHP', 'update JS', 'screenshot', 'update fontkit']);
 
gulp.task('watch', function() {
  // Listen on port 35729
  server.listen(35729, function (err) {
      if (err) {
        return console.log(err)
      };
  
      // Watch files
      gulp.watch('scss/*.scss', ['styles']);
      gulp.watch('scss/**/*.scss', ['styles']);
      gulp.watch('imgs/originals/**', ['images']);
      gulp.watch(['*.php', '**/*.php', '!build/**/*.php'], ['update PHP']);
      gulp.watch('js/*.js', ['update JS']);
      gulp.watch('screenshot.png', ['screenshot']);
      gulp.watch('fontkit/*', ['update fontkit']);
  
    });
 
});var gulp = require('gulp'),
    plumber = require('gulp-plumber'),
    sass = require('gulp-ruby-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    newer = require('gulp-newer'),
    imagemin = require('gulp-imagemin'),
    lr = require('tiny-lr'),
    server = lr()
 
    
gulp.task('styles', function(){
    return sass('scss/*.scss', {
      precision: 6,
      stopOnError: false,
      cacheLocation: 'scss/temp',
      style: 'compressed',
      loadPath: 'scss',
    })
        .pipe(plumber())
        .on('error', sass.logError)
	    .pipe(autoprefixer({browsers:['last 2 versions']}))
	    .pipe(gulp.dest(''))
        .pipe(gulp.dest('build'))
});

gulp.task('update PHP', function(){
	return gulp.src(['*.php', '**/*.php', '!build/**/*.php'])
	    .pipe(plumber())
        .pipe(gulp.dest('build'))
});

gulp.task('update JS', function(){
    return gulp.src('js/*.js')
        .pipe(plumber())
        .pipe(gulp.dest('build/js'))
});
 
var imgSrc = 'imgs/originals/*';
var imgDest = 'imgs';
 
gulp.task('images', function() {
  return gulp.src(imgSrc, {base: 'imgs/originals'})
        .pipe(newer(imgDest))
        .pipe(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true }))
        .pipe(gulp.dest(imgDest))
        .pipe(gulp.dest('build/imgs'))
});

gulp.task('screenshot', function() {
    return gulp.src('screenshot.png')
        .pipe(gulp.dest('build'))
});
 

gulp.task('update fontkit', function(){
    return gulp.src('fontkit/*')
        .pipe(plumber())
        .pipe(gulp.dest('build/fontkit'))
});

gulp.task('default', ['styles', 'images', 'update PHP', 'update JS', 'screenshot', 'update fontkit']);
 
gulp.task('watch', function() {
  // Listen on port 35729
  server.listen(35729, function (err) {
      if (err) {
        return console.log(err)
      };
  
      // Watch files
      gulp.watch('scss/*.scss', ['styles']);
      gulp.watch('scss/**/*.scss', ['styles']);
      gulp.watch('imgs/originals/**', ['images']);
      gulp.watch(['*.php', '**/*.php', '!build/**/*.php'], ['update PHP']);
      gulp.watch('js/*.js', ['update JS']);
      gulp.watch('screenshot.png', ['screenshot']);
      gulp.watch('fontkit/*', ['update fontkit']);
  
    });
 
});