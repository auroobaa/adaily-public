jQuery(document).ready(function($) {
	
	$('[data-womc]').on('submit', function(e) {
		e.preventDefault();
 
		var $form = $(this);
 
		$.post($form.attr('action'), $form.serialize(), function(data) {
            console.log(data.result);
			if (data.result == 'onthelist') {
//                console.log(data.result);
                jQuery("[data-womc-error]").removeClass('hide');
//                alert("alert" + data.result);
            } else if (data.result == 'subscribed' || data.result == 'interest added') {
                jQuery("[data-womc-fields]").addClass('hide');
                jQuery("[data-womc-success]").removeClass('hide');
            } else {
                //do nothing
            }
		}, 'json');
	});
 
});