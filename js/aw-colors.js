jQuery(document).ready(function($) {
    acf.add_filter('color_picker_args', function( args, field ){

        // do something to args
        args.palettes =['#41d5c2', '#61516b', '#ff9676', '#b5c2ca', '#3f3f3f']

        // return
        return args;

    });
});