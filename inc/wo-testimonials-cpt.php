<?php 
/**
 * Creating the Arabic Daily Testimonials
 */
function ad_testimonials_cpt() {
  $labels = array(
    'name'               => _x( 'Testimonials', 'post type general name' ),
    'singular_name'      => _x( 'Testimonial', 'post type singular name' ),
    'add_new'            => _x( 'Add New', 'guest-party' ),
    'add_new_item'       => __( 'Add New Testimonial' ),
    'edit_item'          => __( 'Edit Testimonial' ),
    'new_item'           => __( 'New Testimonial' ),
    'all_items'          => __( 'All Testimonials' ),
    'view_item'          => __( 'View Testimonial' ),
    'search_items'       => __( 'Search Testimonials' ),
    'not_found'          => __( 'No testimonials found' ),
    'not_found_in_trash' => __( 'No testimonials found in the Trash' ), 
    'parent_item_colon'  => '',
    'menu_name'          => 'Testimonials',
  );
    
  $args = array(
    'labels'        => $labels,
    'description'   => 'Arabic Daily Email Optins',
    'public'        => true,
    'supports'		=>	array( 'title', 'editor', 'author', 'revisions', 'excerpt', 'thumbnail' ),
    'menu_position' => 20,
    'menu_icon'     => 'dashicons-welcome-add-page',  
    'has_archive'   => false,
    'hierarchical'  => false,
    'publicly_queryable' => false,
  );
  register_post_type( 'ad-testimonials', $args ); 
}
add_action( 'init', 'ad_testimonials_cpt' );

/**
 * Creating a non-hierarchial taxonomy for lesson vocabulary
 */
function ad_testimonial_types() {
  $labels = array(
    'name'              => _x( 'Types', 'taxonomy general name' ),
    'singular_name'     => _x( 'Type', 'taxonomy singular name' ),
    'search_items'      => __( 'Search Types' ),
    'all_items'         => __( 'All Type' ),
    'parent_item'       => __( 'Parent Type' ),
    'parent_item_colon' => __( 'Parent Type:' ),
    'edit_item'         => __( 'Edit Type' ), 
    'update_item'       => __( 'Update Type' ),
    'add_new_item'      => __( 'Add New Type' ),
    'new_item_name'     => __( 'New Type' ),
    'menu_name'         => __( 'Types' ),
  );
  $args = array(
    'labels' => $labels,
    'hierarchical' => true,
  );
  register_taxonomy( 'ad_testimonial_types', 'ad-testimonials', $args );
}
add_action( 'init', 'ad_testimonial_types', 0 );