<?php
/**
 * Custom functions that act independently of the theme templates.
 *
 * This is functionality added by Aurooba Ahmed
 *
 * @package ArabicDaily_Theme
 */


/**
 * Remove paragraph tags around images
 */
function filter_ptags_on_images($content){
    return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
}
add_filter('the_content', 'filter_ptags_on_images');
add_filter('get_archive_description', 'filter_ptags_on_images');

/**
 * Remove paragraph tags in blockquotes
 */
function filter_ptags_on_blockquotes($content){
    $find = array('<blockquote class="ts-block"><p>', '</p></blockquote>');
    $replace = array('<blockquote class="ts-block">','</blockquote>');
    return str_replace($find, $replace, $content);
}
add_filter('the_content', 'filter_ptags_on_blockquotes');

/**
 * Return ACF sub field without wpautop
 */
function the_sub_field_without_wpautop( $field_name ) {
	
	remove_filter('acf_the_content', 'wpautop');
	
	echo the_sub_field( $field_name );
    
	add_filter('acf_the_content', 'wpautop');
}

/**
 * Deregister WordPress-included jQuery and include Google's
 */
if (!is_admin()) add_action("wp_enqueue_scripts", "wanderoak_jquery_enqueue", 11);
function wanderoak_jquery_enqueue() {
    wp_deregister_script('jquery');
    
    wp_register_script('jquery', "https" . ($_SERVER['SERVER_PORT'] == 443 ? "s" : "") . "://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js", '', null, false);
    
    wp_enqueue_script('jquery');
    
    wp_enqueue_script( 'ad-fontawesome-loader', get_template_directory_uri() . '/js/fontawesome.min.js#asyncload', 'jquery', '', true );
    wp_enqueue_script( 'ad-fontawesome-light', get_template_directory_uri() . '/js/light.min.js#asyncload', 'jquery', '', true );
    wp_enqueue_script( 'ad-fontawesome-regular', get_template_directory_uri() . '/js/regular.min.js#asyncload', 'jquery', '', true );
    wp_enqueue_script( 'ad-fontawesome-solid', get_template_directory_uri() . '/js/solid.min.js#asyncload', 'jquery', '', true );
    wp_enqueue_script( 'ad-fontawesome-brands', get_template_directory_uri() . '/js/brands.min.js#asyncload', 'jquery', '', true );
    wp_enqueue_script( 'ad-flickity', get_template_directory_uri() . '/js/flickity.pkgd.min.js', 'jquery', '010101', true );
    wp_enqueue_script( 'ad-magnific', get_template_directory_uri() . '/js/magnific.js', 'jquery', '010101', true );
    wp_enqueue_script( 'ad-mc', get_template_directory_uri() . '/js/ad-mc.js', 'jquery', '010101', true );
    wp_enqueue_script( 'ad-mc-msg', get_template_directory_uri() . '/js/mc-msg.js', 'jquery', '010101', true );
//    wp_enqueue_script( 'ad-fa-css', get_template_directory_uri() . 'fontawesome-pro-solid.css', 'jquery', '0101010', true );
}

/**
 * Returns a cleaner Navigation
 */

function cleanernav($location) {
    $clearnav = wp_nav_menu(array(
                    'theme_location' => $location,
                    'container' => false,
                    'items_wrap' => '%3$s', 
                    'echo' => false,
                    ));

    $find = array('><a', '<li');
    $replace = array('','<a');
    $newnav = str_replace($find, $replace, $clearnav);
    echo $newnav;
}

/**
 * ACF Theme Options Page
 */
//if( function_exists('acf_add_options_page') ) {
//	
//	acf_add_options_page();
//	acf_set_options_page_title( __('Theme Options') );
//}
if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Options',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false,
        'parent_slug'	=> 'themes.php'
	));
	acf_add_options_page(array(
		'page_title' 	=> 'Global Information',
		'menu_title'	=> 'Global Info',
		'menu_slug' 	=> 'global-info',
		'capability'	=> 'edit_posts',
		'redirect'		=> false,
        'parent_slug'	=> '',
        'position'      => '2.13',
        'icon_url'      => 'dashicons-admin-site',
	));
	
}
/**
 * Numbered pagination
 */
function pagination_bar() {
    global $wp_query;
 
    $total_pages = $wp_query->max_num_pages;
 
    if ($total_pages > 1){
        $current_page = max(1, get_query_var('paged'));
 
        echo paginate_links(array(
            'base' => get_pagenum_link(1) . '%_%',
            'format' => '/page/%#%',
            'current' => $current_page,
            'total' => $total_pages,
        ));
    }
}


/**
* Remove prefix on archive pages
*/

add_filter( 'get_the_archive_title', function ($title) {

    if ( is_category() ) {

            $title = single_cat_title( '', false );

        } elseif ( is_tag() ) {

            $title = single_tag_title( '', false );

        } elseif ( is_author() ) {

            $title = '<span class="vcard">' . get_the_author() . '</span>' ;

        }

    return $title;

});

/**
* Include editor style stylesheet.
*/
add_editor_style('editor-style.css');

/**
* Hide WordPress Update Nag to All But Admins
*/
function hide_update_notice_to_all_but_admin() {
    if ( !current_user_can( 'update_core' ) ) {
        remove_action( 'admin_notices', 'update_nag', 3 );
    }
}
add_action( 'admin_head', 'hide_update_notice_to_all_but_admin', 1 );

/**
* Disable the Emoji Nonsense
*/
function disable_wp_emojicons() {
  remove_action( 'admin_print_styles', 'print_emoji_styles' );
  remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
  remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
  remove_action( 'wp_print_styles', 'print_emoji_styles' );
  remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
  remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
  remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
  add_filter( 'tiny_mce_plugins', 'disable_emojicons_tinymce' );
}
add_action( 'init', 'disable_wp_emojicons' );
function disable_emojicons_tinymce( $plugins ) {
  if ( is_array( $plugins ) ) {
    return array_diff( $plugins, array( 'wpemoji' ) );
  } else {
    return array();
  }
}

/**
* Media should also have categories, don't you agree?
*/

function add_categories_for_attachments() {
    register_taxonomy_for_object_type( 'category', 'attachment' );
}
add_action( 'init' , 'add_categories_for_attachments' );


/**
* No one needs the xmlrpc vulnerability.
*/
add_filter( 'xmlrpc_enabled', '__return_false' );
remove_action( 'wp_head', 'rsd_link' );
remove_action( 'wp_head', 'wlwmanifest_link' );

/**
* Custom Social Media Nav Walker
*/

class Nav_Social_Walker extends Walker_Nav_Menu {
	function start_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = str_repeat("\t", $depth);
		$output .= "\n$indent\n";
	}
	function end_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = str_repeat("\t", $depth);
		$output .= "$indent\n";
	}
	function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
		$class_names = $value = '';
		$classes = empty( $item->classes ) ? array() : (array) $item->classes;
		$classes[] = 'menu-item-' . $item->ID;
		$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
		$class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';
		$id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
		$id = $id ? ' id="' . esc_attr( $id ) . '"' : '';
		$output .= $indent . '';
		$attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
		$attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
		$attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
		$attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
		$item_output = $args->before;
        if (strpos($item->url, 'facebook') !== false) {
            $item_output .= '<a'. $attributes .'><i class="fab fa-facebook-f"></i>';
//            $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
            $item_output .= '</a>';
            $item_output .= $args->after;
        } elseif (strpos($item->url, 'twitter') !== false)  {
            $item_output .= '<a'. $attributes .'><i class="fab fa-twitter">';
//            $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
            $item_output .= '</i></a>';
            $item_output .= $args->after;
        } elseif (strpos($item->url, 'instagram') !== false)  {
            $item_output .= '<a'. $attributes .'><i class="fab fa-instagram">';
            $item_output .= '</i></a>';
            $item_output .= $args->after;
        } elseif (strpos($item->url, 'youtube') !== false)  {
            $item_output .= '<a'. $attributes .'><i class="fab fa-youtube-play">';
            $item_output .= '</i></a>';
            $item_output .= $args->after;
        }  elseif (strpos($item->url, 'snapchat') !== false)  {
            $item_output .= '<a'. $attributes .'><i class="fab fa-snapchat">';
            $item_output .= '</i></a>';
            $item_output .= $args->after;
        } elseif (strpos($item->url, 'vimeo') !== false)  {
            $item_output .= '<a'. $attributes .'><i class="fab fa-vimeo-v">';
            $item_output .= '</i></a>';
            $item_output .= $args->after;
        } 
		
		
		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}
	function end_el( &$output, $item, $depth = 0, $args = array() ) {
		$output .= "\n";
	}
}

/** 
 * Add async to any js script with #asyncload on the end of the URL
 * Credit goes to: https://ikreativ.com/async-with-wordpress-enqueue/
 */

function wo_async_scripts($url)
{
    if ( strpos( $url, '#asyncload') === false )
        return $url;
    else if ( is_admin() )
        return str_replace( '#asyncload', '', $url );
    else
	return str_replace( '#asyncload', '', $url )."' defer"; 
    }
add_filter( 'clean_url', 'wo_async_scripts', 11, 1 );

add_action( 'plugins_loaded', 'remove_hmn_comments_template', 100 );

function remove_hmn_comments_template() {
//    global $comment_popularity;
	$comment_popularity = CommentPopularity\HMN_Comment_Popularity::get_instance();
    $comment_popularity1 = MN_Comment_Popularity::get_instance();
    if (class_exists('HMN_Comment_Popularity')) {
        
        remove_filter( 'comments_template', array( $comment_popularity1, 'custom_comments_template' ) );
        
    }
}

function wo_colour_class($hexcode) {
    $colours = array (
        'primary' => '#41d5c2',
        'secondary' => '#61516b',
        'highlighter' => '#ff9676',
        'detail' => '#3f3f3f',
        'light' => '#eae9e8',
        'darklight' => '#b5c2ca',
        'bg' => '#f7f5f2',
        'darkprimary' => '#31A092',
    );
    $class = array_search($hexcode, $colours);
    return 'colour-' . $class;
}

add_filter('learndash_completion_redirect', 'course_page_redirection', 5, 2);

function course_page_redirection($link, $postid) {
    global $post;
    $current_class = get_body_class();
    if ($current_class[0] == 'sfwd-courses-template-default') {
        $link = get_permalink($post->ID);
        return $link;
    }
    return $link;
}

function wo_ad_searchfilter($query) {
 
    if ($query->is_search && !is_admin() ) {
        $query->set('post_type',array( 'post', 'page', 'sfwd-courses' ));
    }
 
    return $query;
}
 
add_filter('pre_get_posts','wo_ad_searchfilter');

function wpsites_change_comment_form_submit_label($arg) {
$arg['label_submit'] = 'Submit';
return $arg;
}
add_filter('comment_form_defaults', 'wpsites_change_comment_form_submit_label');


function ad_confirm_mark_complete( $post ) {
    //make a div that's half
    $initial_message = '<span class="mark-complete-message">Mark Complete</span>';
    $mark_complete = learndash_mark_complete($post);
    $confirmation = '<div class="ad-confirm-mark-complete-wrapper"><div class="ad-confirm-mark-complete"><span class="label">Are you sure?</span>' . $mark_complete . '<span class="no-complete">No</span></div></div>';
    $full_markup = $initial_message . $confirmation;
    echo $full_markup;
}

add_action("wp_enqueue_scripts", "confirm_mark_complete_script", 11);
function confirm_mark_complete_script() {
     wp_enqueue_script( 'ad-confirm-mark-complete-script', get_template_directory_uri() . '/js/ad-confirm-mark-complete.js', 'jquery', '', true );
}

function ad_comments( $comment, $args, $depth ) {
     $GLOBALS['comment'] = $comment; 

//echo '<pre>' . var_export($comment->get_children(), true) . '</pre>';
    if ($comment->populated_children == 'true') : echo 'YOUYOUOUOU'; endif;
?>
     
    <li id="comment-<?php comment_ID() ?>" <?php comment_class( ($comment->get_children()) ? 'parent' : '' ); ?>>
        <article id="div-comment-<?php comment_ID() ?>" class="comment-body">
            <div class="comment-content">
                <?php comment_text(); ?>
            </div>
            <footer class="comment-meta">
                <div class="comment-author"> 
                    <a class="comment-permalink" href="<?php echo htmlspecialchars ( get_comment_link( $comment->comment_ID ) ) ?>"><?php printf(__('%1$s'), get_comment_date(), get_comment_time()) ?></a>
                    <span>by</span> 
                    <span class="comment-author-name">
                        <?php printf(__('%s'), get_comment_author_link()) ?>
                    </span>
                </div>
                
                <div class="reply">
                    <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
                </div>
            </footer>

            <?php if ($comment->comment_approved == '0') : ?>
                <em><php _e('Your comment is awaiting moderation.') ?></em><br />
            <?php endif; ?>
        </article>
<?php }