<?php

use \DrewM\MailChimp\MailChimp;

$ssl = true;
new wo_mc_acf();
	
	class wo_mc_acf {
		
		public function __construct() {
//            global $post;
//            global $hook_suffix;
//            if ($hook_suffix != 'post.php') {
//                return;
//            }
			// choose_list field on page builder
			add_action('acf/load_field/key=field_5b16d21f1b1f7', array($this, 'load_mc_list_choices'));
			// choose_interest field on page builder
			add_action('acf/load_field/key=field_5b16d22e1b1f8', array($this, 'load_mc_interest'));
			// choose_group field on page builder
			add_action('acf/load_field/key=field_5b16d24a1b1f9', array($this, 'load_mc_groups'));
			// ajax action for loading interest choices
			add_action('wp_ajax_load_mc_interest', array($this, 'ajax_load_mc_interest'));
            // ajax action for loading group choices
			add_action('wp_ajax_load_mc_groups', array($this, 'ajax_load_mc_groups'));
			// enqueue js extension for acf
			// do this when ACF is enqueuing scripts
			add_action('acf/input/admin_enqueue_scripts', array($this, 'enqueue_script'));
		} // end public function __construct
		
		public function load_mc_list_choices($field) {
			// this funciton dynamically loads the state select field choices
			// from the state post type
			
			// I only want to do this on Posts and theme options
			global $post;
            global $hook_suffix;

			if (!$post ||
			    !isset($post->ID) ||
			    (get_post_type($post->ID) != 'ad-mcoptin') ) {
                    return $field;
				
			}
			
            $apikey = get_field('api_key', 'option');

            $MailChimp = new MailChimp($apikey);

            $MailChimp->verify_ssl = $ssl;


            $result = $MailChimp->get('lists');

            $lists = array('' => '-- Lists --');
            foreach ($result['lists'] as $list) {

                $lists[$list['id']] = $list['name'];
            }
//            var_dump($lists);
			$field['choices'] = $lists;
			return $field;
		} // end public function load_state_field_choices
		
		public function load_mc_interest($field) {
			// this function dynamically loads city field choices
			// based on the currently saved state
			
			// I only want to do this on Posts
			global $post;
            global $hook_suffix;

			if (!$post ||
			    !isset($post->ID) ||
			    (get_post_type($post->ID) != 'ad-mcoptin') ) {
                    return $field;

				
			}
			// get the state post id
			// I generally use get_post_meta() instead of get_field()
			// when building functionality, but get_field() could be
			// subsitited here
//            if ($hook_suffix != 'appearance_page_theme-general-settings') {
                $list = get_post_meta($post->ID, 'choose_list', true);
//            }
			
//            $list = get_sub_field('choose_list', $post->ID);
//                $list = '1b19f3e14a';
//            var_dump($list);
			$interests = $this->get_interests($list);
			$field['choices'] = $interests;
            
			return $field;
		} // end public funciton load_city_field_choices
        
		public function load_mc_groups($field) {
			// this function dynamically loads city field choices
			// based on the currently saved state
			
			// I only want to do this on Posts
			global $post;
            global $hook_suffix;

			if (!$post ||
			    !isset($post->ID) ||
			    (get_post_type($post->ID) != 'ad-mcoptin') ) {
                    return $field;
                
				
			}
			// get the state post id
			// I generally use get_post_meta() instead of get_field()
			// when building functionality, but get_field() could be
			// substituted here
//            if ($hook_suffix != 'appearance_page_theme-general-settings') {
                $list = get_post_meta($post->ID, 'choose_list', true);
                $interest_id = get_post_meta($post->ID, 'category', true);
//            } 

//            $list = get_sub_field('choose_list', $post->ID);
//                $list = '1b19f3e14a';
//            var_dump($list);
			$groups = $this->get_groups($list, $interest_id);
			$field['choices'] = $groups;
			return $field;
		} // end public funciton load_city_field_choices
		
		public function enqueue_script() {
			// enqueue acf extenstion
			
			// only enqueue the script on the post page where it needs to run
			/* *** THIS IS IMPORTANT
			       ACF uses the same scripts as well as the same field identification
			       markup (the data-key attribute) if the ACF field group editor
			       because of this, if you load and run your custom javascript on
			       the field group editor page it can have unintended side effects
			       on this page. It is important to alway make sure you're only
			       loading scripts where you need them.
			*/
//			global $post;
//			if (!$post ||
//			    !isset($post->ID) || 
//			    get_post_type($post->ID) != 'post') {
//				return;
//			}
            
			global $post;
            global $hook_suffix;

			if (!$post ||
			    !isset($post->ID) ||
			    (get_post_type($post->ID) != 'ad-mcoptin') ) {
                    return $field;

				
			}
			
			$handle = 'wo_mc_acf';
			
			// I'm using this method to set the src because
			// I don't know where this file will be located
			// you should alter this to use the correct fundtions
			// to set the src value to point to the javascript file
//            $src = get_template_directory() . '/js/ad-mailchimp-acf.js';

            $src = get_template_directory_uri() .'/js/ad-mc.js';
//			$src = '/'.str_replace(ABSPATH, '', dirname(__FILE__)).'/js/ad-mailchimp-acf.js';
			// make this script dependent on acf-input
			$depends = array('acf-input');
			
			wp_enqueue_script($handle, $src, $depends);
		} // end public function enqueue_script

		public function ajax_load_mc_interest() {
			// this funtion is called by AJAX to load cities
			// based on state selecteion
			
			// we can use the acf nonce to verify
			if (!wp_verify_nonce($_POST['nonce'], 'acf_nonce')) {
				die();
			}
			$list = 0;
			if (isset($_POST['list'])) {
				$list = $_POST['list'];
			}
//            var_dump($_POST);
//            $list = '1b19f3e14a';
			$interests = $this->get_interests($list);
			$choices = array();
			foreach ($interests as $value => $label) {
				$choices[] = array('value' => $value, 'label' => $label);
			}
//            echo json_encode($_POST);
			echo json_encode($choices);
			exit;
		} // end public function ajax_load_city_field_choices
        
		public function ajax_load_mc_groups() {
			// this funtion is called by AJAX to load cities
			// based on state selecteion
			
			// we can use the acf nonce to verify
			if (!wp_verify_nonce($_POST['nonce'], 'acf_nonce')) {
				die();
			}
			$list = 0;
            $interests = 0;
			if (isset($_POST['list'])) {
				$list = $_POST['list'];
			}
			if (isset($_POST['interests'])) {
				$interests = $_POST['interests'];
			}
            
//            var_dump($_POST);
//            $list = '1b19f3e14a';
			$groups = $this->get_groups($list, $interests);
			$choices = array();
			foreach ($groups as $value => $label) {
				$choices[] = array('value' => $value, 'label' => $label);
			}
//            echo json_encode($_POST);
			echo json_encode($choices);
			exit;
		} // end public function ajax_load_city_field_choices
		
		private function get_interests($list_id) {
			// $list_id is ID of selected list
			// get all interest groups in that list's first category
            $apikey = get_field('api_key', 'option');

            $MailChimp = new MailChimp($apikey);

            $MailChimp->verify_ssl = $ssl;
            
            $categories = $MailChimp->get("lists/$list_id/interest-categories");
            $thecats = $categories['categories'];
            
            $interests = array('' => '-- Categories --');
            if (!$categories['categories']) :
                $interests[1] = 'No Categories in this List';
            else :
                foreach ($thecats as $category) {
                    $interests[$category['id']] = $category['title'];
                }
            endif;

//            var_dump($list_id);
			return $interests;
            
		} // end private function get_interests
        
		private function get_groups($list_id, $interest_id) {
			// $list_id is ID of selected list
			// get all interest groups in that list's first category
            $apikey = get_field('api_key', 'option');

            $MailChimp = new MailChimp($apikey);

            $MailChimp->verify_ssl = $ssl;

            $groups = $MailChimp->get("lists/$list_id/interest-categories/$interest_id/interests");
            
            $thegroups = $groups['interests'];

            $groups_list = array('' => '-- Groups --');
            if (!$thegroups) :
                $groups_list[1] = 'No groups in this List';
            else :
                foreach ($thegroups as $group) {
                    $groups_list[$group['id']] = $group['name'];
                }
            endif;

//            var_dump($list_id);
//            var_dump($interests);
			return $groups_list;
            
		} // end private function get_groups
		
	} // end class wo_mc_acf

add_action( 'wp_ajax_womc_subscribe', 'womc_subscribe' );
add_action( 'wp_ajax_nopriv_womc_subscribe', 'womc_subscribe' );
function womc_subscribe() {
    if ( 
        ! isset( $_POST['post_mc_sub'] ) 
        || ! wp_verify_nonce( $_POST['post_mc_sub'], 'womc_subscribe_nonce') 
    ) {
 
        exit('The form is not valid');
 
    }
    // A default response holder, which will have data for sending back to our js file
    $response = array(
    	'error' => false,
        'result' => false,
    );
 
    // Example for creating an response with error information, to know in our js file
    // about the error and behave accordingly, like adding error message to the form with JS
//    if (trim($_POST['email']) == '') {
//    	$response['error'] = true;
//    	$response['error_message'] = 'Email is required';
// 
//    	// Exit here, for not processing further because of the error
//    	exit(json_encode($response));
//    }
 
    // ... Do some code here, like storing inputs to the database, but don't forget to properly sanitize input data!
    $apikey = get_field('api_key', 'option');

    $MailChimp = new MailChimp($apikey);

    $MailChimp->verify_ssl = $ssl;    
    
    $list_id = $_POST['list_id'];
    $email_address = $_POST['email'];
    $interests = $_POST['group_id'];
    $signup_title = $_POST['signup'];
    $url = $_POST['_wp_http_referer'];
    
    
//    $result = $MailChimp->post("lists/$list_id/members", [
//				"email_address" => "hala@auroobaahmed.com",
//				"status"        => "subscribed",
//			]);
    
    //We're checking to see if this person is already subscribed, if they are, then just update their info with the new interest, if not then subscribe them.
    $subscriber_hash = $MailChimp->subscriberHash($email_address);
    $getresult = $MailChimp->get("lists/$list_id/members/$subscriber_hash");
    
    
    if ($interests) :    
        // If this optin has an interest that needs be added
    
        //if this subscriber doesn't exist, add them with the interest and then say 'subscribed'
        if ($getresult['status'] == '404') {
            $result = $MailChimp->post("lists/$list_id/members", [
                            'email_address' => $email_address,
                            'status'        => 'subscribed',
                            'interests'    => [$interests => true],
                            'merge_fields' => ['SIGNUP' => $signup_title,
                                               'URL' => $url,
                                              ],
                        ]);
            $response['result'] = 'subscribed';

        } else {
            //if this subscriber does exist then add the interest, but if the interest already exists, then say 'onthelist'
            if (!$getresult['interests'][$interests]) {
                $result = $MailChimp->patch("lists/$list_id/members/$subscriber_hash", [
                                'interests'    => [$interests => true],
                            ]);
                $response['result'] = 'interest added';   
            } else {
                $response['result'] = 'onthelist';
            }
        }
    else :
        // If this optin doesn't have an interest that needs to be added
    
        //if this subscriber doesn't exist, add them and then say 'subscribed'
        if ($getresult['status'] == '404') {
            $result = $MailChimp->post("lists/$list_id/members", [
                            'email_address' => $email_address,
                            'status'        => 'subscribed',
                            'merge_fields' => ['SIGNUP' => $signup_title],
                        ]);
            $response['result'] = 'subscribed';
        } else {
            // If they already exist, do nothing and pass on 'onthelist'    
        
            $response['result'] = 'onthelist';
        }
    endif;
    
    
        

    if (!$response['result']) {
        $response['result'] = $MailChimp->getLastError();
        
    }
    // Don't forget to exit at the end of processing
    exit(json_encode($response));
}

