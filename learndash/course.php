<?php
/**
 * Displays a course
 *
 * Available Variables:
 * $course_id 		: (int) ID of the course
 * $course 		: (object) Post object of the course
 * $course_settings : (array) Settings specific to current course
 * 
 * $courses_options : Options/Settings as configured on Course Options page
 * $lessons_options : Options/Settings as configured on Lessons Options page
 * $quizzes_options : Options/Settings as configured on Quiz Options page
 * 
 * $user_id 		: Current User ID
 * $logged_in 		: User is logged in
 * $current_user 	: (object) Currently logged in user object
 * 
 * $course_status 	: Course Status
 * $has_access 	: User has access to course or is enrolled.
 * $materials 		: Course Materials
 * $has_course_content		: Course has course content
 * $lessons 		: Lessons Array
 * $quizzes 		: Quizzes Array
 * $lesson_progression_enabled 	: (true/false)
 * $has_topics		: (true/false) 
 * $lesson_topics	: (array) lessons topics 
 * 
 * @since 2.1.0
 * 
 * @package ArabicDaily_Theme
 */
?>


<?php
/**
 * Display course status
 */
?>
<?php if ( $logged_in ) : ?>
<!--
	<span id="learndash_course_status">
		<b><?php printf( _x( '%s Status:', 'Course Status Label', 'learndash' ), LearnDash_Custom_Label::get_label( 'course' ) ); ?></b> <?php echo $course_status; ?>
		<br />
	</span>
	<br />
-->

	<?php 
		/** 
		 * Filter to add custom content after the Course Status section of the Course template output. 
		 * @since 2.3
		 * See https://bitbucket.org/snippets/learndash/7oe9K for example use of this filter. 
		 */
		echo apply_filters('ld_after_course_status_template_container', '', learndash_course_status_idx( $course_status ), $course_id, $user_id ); 
	?>

<?php endif; ?>

<div class="ad-course-description"><?php echo $content; ?></div>
<?php
$workbook_type = get_field('course_workbook_type');
if ($workbook_type == 'file'):
    $workbook = get_field('course_workbook_file');
else :
    $workbook = get_field('course_workbook_link');
endif;
$calendar = get_field('course_calendar');

?>
<?php if ( $logged_in && !$has_access ) : ?>
	<?php 
    $workbook_type_trial = get_field('course_workbook_type_trial');
    if ($workbook_type_trial == 'file'):
        $workbook_trial = get_field('course_workbook_file_trial');
    else :
        $workbook_trial = get_field('course_workbook_link_trial');
    endif;
    ?>
    <section class="ad-course-materials">
       <?php if ($workbook_trial) : ?>
        <div class="course-workbook button-wrapper">
            <a class="button button-orange" href="<?php echo $workbook_trial; ?>" download>Download Sample Course Workbook</a>
        </div>
        <?php endif; ?>
    </section>
<?php endif; ?>
<?php if ( $has_access) : ?>      
<section class="ad-course-materials">
   <?php if ($workbook) : ?>
    <div class="course-workbook button-wrapper">
        <a class="button button-orange" href="<?php echo $workbook; ?>" download>Download Course Workbook</a>
    </div>
    <?php endif; ?>
    <?php if ($calendar) : ?>
    <div class="course-calendar button-wrapper">
        <a class="button button-orange" href="<?php echo $calendar; ?>" download>Download Course Calendar</a>
    </div>
    <?php endif; ?>
</section>
<?php endif; ?>
<?php if ( $logged_in ) : ?>
	<?php  if ( ! empty( $course_certficate_link ) ) : ?>
		<div id="ad_learndash_course_certificate" class="ad_learndash_course_certificate">
		    <p>You completed the course!</p>
		    <div class="button-wrapper">
			<a href='<?php echo esc_attr( $course_certficate_link ); ?>' class="button" target="_blank"><?php echo apply_filters('ld_certificate_link_label', __( 'Print Your Certificate', 'learndash' ), $user_id, $post->ID ); ?></a></div>
		</div>
		<br />
	<?php endif; ?>
<?php endif; ?>
<?php if ( $has_course_content ) : ?>
	<?php 
		$show_course_content = true;
		if ( !$has_access ) :
			if ( $course_meta['sfwd-courses_course_disable_content_table'] == 'on' ) :
				$show_course_content = false;
			endif;	
		endif;
		
		if ( $show_course_content ) :
			?>
	<div id="learndash_course_content" class="learndash_course_content">
<!--			<h4 id="learndash_course_content_title"><?php printf( _x( '%s Content', 'Course Content Label', 'learndash' ), LearnDash_Custom_Label::get_label( 'course' ) ); ?></h4>-->

		<?php
        /**
         * Display lesson list
         */
        ?>
		<?php if ( ! empty( $lessons ) ) : ?>

			<?php if ( $has_topics ) : ?>
				<div class="expand_collapse">
					<a href="#" onClick='jQuery("#learndash_post_<?php echo $course_id; ?> .learndash_topic_dots").slideDown(); return false;'><?php _e( 'Expand All', 'learndash' ); ?></a> | <a href="#" onClick='jQuery("#learndash_post_<?php echo esc_attr( $course_id ); ?> .learndash_topic_dots").slideUp(); return false;'><?php _e( 'Collapse All', 'learndash' ); ?></a>
				</div>
				<?php if ( apply_filters('learndash_course_steps_expand_all', false, $course_id, 'course_lessons_listing_main' ) ) { ?>
					<script>
						jQuery(document).ready(function(){
							jQuery("#learndash_post_<?php echo $course_id; ?> .learndash_topic_dots").slideDown();
						});
					</script>	
				<?php } ?>
			<?php endif; ?>

			<div id="ad-learndash_lessons" class="ad-learndash_lessons">

<!--
				<div id="lesson_heading">
						<span><?php echo LearnDash_Custom_Label::get_label( 'lessons' ) ?></span>
					<span class="right"><?php _e( 'Status', 'learndash' ); ?></span>
				</div>
-->
				
				
                <?php
                    $sectioned = array();
                    foreach ( $lessons as $lesson ) {
//                        echo '<pre>' . var_export(get_the_terms($lesson['post']->ID, 'ld_lesson_category'), true) . '</pre>';
                        $post_settings = learndash_get_setting($lesson['post']->ID);
                        $section_id = $post_settings['associated_section'];
                        $section_label = get_the_title($section_id);
                        
                        $lesson_list = array();
                        if (!isset($sectioned[$section_label])) {
                            $lesson_list[] = $lesson;
                            $sectioned[$section_label]['lesson_list'] = $lesson_list;
                            $sectioned[$section_label]['section_id'] = $section_id;
                        } else {
                            $lesson_list = $sectioned[$section_label]['lesson_list'];
                            $lesson_list[] = $lesson;
                            $sectioned[$section_label]['lesson_list'] = $lesson_list;
                        }
                    } 

                ?>

                <?php 
                if ( ! empty( $quizzes ) ) :
                

                    foreach ($quizzes as $quiz) {
                        $quiz_settings = learndash_get_setting($quiz['post']->ID);
//                        echo '<pre>' . var_export($quiz_settings, true) . '</pre>';
                        if ($quiz_settings['associated_section'] != 0) :
                            $section_id = $quiz_settings['associated_section'];
                            $section_label = get_the_title($section_id);
                            $quiz_list = array();
                            $quiz_list[] = $quiz;
                            $sectioned[$section_label]['quiz_list'] = $quiz_list;
                        elseif ($quiz_settings['associated_section'] == 0) :
                            $main_quizzes = array();
                            $main_quizzes[] = $quiz;
                        endif; 
                    }

                endif;
//                    echo '<pre>' . var_export($sectioned, true) . '</pre>';
                ?>
                <?php
                foreach ( $sectioned as $section => $content_list ) {
                ?>
                    <section class="ad-course-section">
                        <header>
                            <div class="title-wrapper">
                                <div><h2><?php echo get_the_title($content_list['section_id']); ?></h2></div>
                            </div>
                            <div class="section-description-wrapper">
                                <p><?php echo get_field('section_description', $content_list['section_id'])?></p>
                            </div>
                        </header>
                        <?php
                            $lesson_list = $content_list['lesson_list'];
                            foreach ($lesson_list as $lesson_item) { 
                                $taxonomy = get_the_terms($lesson_item['post']->ID, 'ld_lesson_category');
                                $icon_type = get_field('icon_type', 'ld_lesson_category_' . $taxonomy[0]->term_id );
                                    if ($icon_type == 'icon') {
                                        $icon = get_field('icon', 'ld_lesson_category_' . $taxonomy[0]->term_id );
                                        $icon = '<i class="fas ' . $icon . '"></i>';
                                    } else {
                                        $icon = get_field('lesson_letter', 'ld_lesson_category_' . $taxonomy[0]->term_id );
                                    }
                         
                        ?>
                               <article class="ad-lesson <?php if (! empty( $lesson_item['lesson_access_from'])): echo 'not-available'; endif; if ((!$has_access || ! empty( $lesson_item['lesson_access_from'])) && $lesson_item['sample'] == 'is_not_sample' || !$logged_in) : echo 'ad-lesson-no-access'; endif; ?> ">
                                      
                                   <div class="ad-lesson-icon <?php if ($icon_type == 'letter'): echo 'letter-icon'; endif; ?>">
                                       <?php echo $icon; ?> 
                                   </div>
                                   <?php //var_dump($lesson_item['sample']); ?>
                                   <?php if ((!$has_access || ! empty( $lesson_item['lesson_access_from'])) && $lesson_item['sample'] == 'is_not_sample') : ?>
                                       <div class="ad-lesson-title">
                                       <?php echo $lesson_item['post']->post_title; ?>
                                       </div>
                                   <?php elseif (($lesson_item['sample'] == 'is_sample') && $logged_in ) : ?>
                                       <a href='<?php echo esc_attr( $lesson_item['permalink'] ); ?>' class="ad-lesson-title">
                                           <?php echo $lesson_item['post']->post_title;; ?>
                                       </a>
                                   <?php elseif ($logged_in && $has_access) : ?>
                                       <a href='<?php echo esc_attr( $lesson_item['permalink'] ); ?>' class="ad-lesson-title">
                                           <?php echo $lesson_item['post']->post_title;; ?>
                                       </a>
                                   <?php else: ?>
                                        <div class="ad-lesson-title">
                                           <?php echo $lesson_item['post']->post_title;; ?>
                                       </div>
                                   <?php endif; ?>
<!--                                   <div class="ad-lesson-status-wrapper">-->
                                       <div class="ad-lesson-status">
                                       <?php if (!$logged_in):
                                            echo '<span class="locked-lesson">Locked</span>';

                                            elseif ($lesson_item['status'] == 'completed') :
                                        ?>
                                            <span>Complete<i class="fas fa-check"></i></span>
                                        <?php elseif (($lesson_item['status'] == 'notcompleted')) : ?>
                                            <?php if ( !$has_access && ($lesson_item['sample'] != 'is_sample') ) {
                                                echo '<span class="locked-lesson">Locked</span>';
                                            } else {
    //                                            echo learndash_mark_complete( $lesson_item['post'] );
                                                    ad_confirm_mark_complete($lesson_item['post'] );
                                            }  ?>
                                        <?php elseif (! empty( $lesson_item['lesson_access_from']) || !$has_access) : ?>
                                            <?php echo '<span class="locked-lesson">Locked</span>'; ?>
                                        <?php endif; ?>
                                       </div>
<!--                                   </div>-->
                                   	<?php
                                    /**
                                     * Lesson Topics
                                     */
                                    ?>
                                    <?php $topics = $lesson_topics[ $lesson['post']->ID ]; ?>

                                    <?php if ( ! empty( $topics ) ) : ?>
                                        <div id='learndash_topic_dots-<?php echo esc_attr( $lesson['post']->ID ); ?>' class="learndash_topic_dots type-list">
                                            <ul>
                                                <?php $odd_class = ''; ?>
                                                <?php foreach ( $topics as $key => $topic ) : ?>
                                                    <?php $odd_class = empty( $odd_class ) ? 'nth-of-type-odd' : ''; ?>
                                                    <?php $completed_class = empty( $topic->completed ) ? 'topic-notcompleted':'topic-completed'; ?>												
                                                    <li class='<?php echo esc_attr( $odd_class ); ?>'>
                                                        <span class="topic_item">
                                                            <a class='<?php echo esc_attr( $completed_class ); ?>' href='<?php echo esc_attr( get_permalink( $topic->ID ) ); ?>' title='<?php echo esc_attr( $topic->post_title ); ?>'>
                                                                <span><?php echo $topic->post_title; ?></span>
                                                            </a>
                                                        </span>
                                                    </li>
                                                <?php endforeach; ?>
                                            </ul>
                                        </div>
                                    <?php endif; ?>
                               </article>
                               <?php
                                
                            }
                        ?>
                        <?php
//                    echo '<pre>' . var_export($sectioned, true) . '</pre>';
                            if (isset($content_list['quiz_list'])) :
                                $quiz_list = $content_list['quiz_list'];
                    
                                foreach ($quiz_list as $quiz) { 
                                    $icon = get_field('quiz_icon', 'option' );
                                ?>
                                    <article class="ad-lesson">
                                        <div class="ad-lesson-icon">
                                            <i class="fas <?php echo $icon; ?>"></i>
                                       </div>
                                    <?php if (!$has_access) : ?>
                                    <div class="ad-lesson-title">
                                    <?php echo $quiz['post']->post_title;; ?>
                                    </div>
                                    <?php else : ?>
                                    <a href='<?php echo esc_attr( $quiz['permalink'] ); ?>' class="ad-lesson-title">
                                       <?php echo $quiz['post']->post_title;; ?>
                                    </a>
                                    <?php endif; ?>
                                    <div class="ad-lesson-status">
                                     <?php
                            if ($quiz['status'] == 'completed') :
                        ?>
                            <span>Complete<i class="fas fa-check"></i></span>
                        <?php elseif (!$has_access || !$logged_in) : ?>
                            <?php echo '<span class="locked-lesson">Locked</span>'; ?>

                        <?php elseif ($quiz['status'] == 'notcompleted') :  ?>
                            <a href='<?php echo esc_attr( $quiz['permalink'] ); ?>' class="">
                                Take Quiz
                            </a>
                            
                        <?php endif; ?>
                                   </div>
                                    </article>
                                <?php }
                            endif;
                            if (isset($content_list['quiz_list'])) :
                                $quiz_list = $content_list['quiz_list'];
                                foreach ($quizzes as $quiz) {
//                                    echo '<article><a href="' . esc_attr( $quiz['permalink'] ) . '">' . $quiz['post']->post_title . '</a></article>';
                                }
                            endif;
                        ?>
                    </section>
                <?php 
                }
                ?>
                <section class="final-quiz">
                   <?php
//                    var_dump($main_quizzes);
                    ?>
                   
                    <?php if (!empty($main_quizzes)) : 
                              foreach ($main_quizzes as $quiz) { 
                        $icon = get_field('quiz_icon', 'option' );?>
                    <article class="ad-lesson ad-main-quiz  <?php if (!$has_access) : echo 'ad-lesson-no-access'; endif ?>">
                        <div class="ad-lesson-icon">
                            <i class="fas <?php echo $icon; ?>"></i>
                        </div>
                        <?php if (!$has_access) : ?>
                            <div class="ad-lesson-title">
                            <?php echo $quiz['post']->post_title; ?>
                            </div>
                        <?php else : ?>
                            <a href='<?php echo esc_attr( $quiz['permalink'] ); ?>' class="ad-lesson-title">
                               <?php echo $quiz['post']->post_title;; ?>
                            </a>
                        <?php endif; ?>
                        <div class="ad-lesson-status">
                        <?php
                            if ($quiz['status'] == 'completed') :
                        ?>
                            <span>Complete<i class="fas fa-check"></i></span>
                        <?php elseif (!$has_access || !$logged_in) : ?>
                            <?php echo '<span class="locked-lesson">Locked</span>'; ?>

                        <?php elseif ($quiz['status'] == 'notcompleted') :  ?>
                            <a href='<?php echo esc_attr( $quiz['permalink'] ); ?>' class="">
                                Take Quiz
                            </a>
                            
                        <?php endif; ?>
                       </div>
                    </article>
                    <?php } endif; ?>
                </section>
				<div id="lessons_list" class="lessons_list">

                
                
                
                
                
					<?php foreach ( $lessons as $lesson ) : ?>


                        <?php $getPostCustom=get_post_custom($lesson['post']->ID); 
                    
                    $post_settings = learndash_get_setting($lesson['post']->ID);
//                    $section_id = $post_settings['associated_section'];
//                    $section_label = get_the_title($section_id);
//                    echo '<h2 style="font-size: 42px;">'. $section_label .'</h2>';
                    
          // Get all the data ?>
<!--					    <h3>All Post Meta</h3>-->
                        <?php
                    
//                            foreach($getPostCustom as $name=>$value) {
//
//                                echo "<strong>".$name."</strong>"."  =>  ";
//
//                                foreach($value as $nameAr=>$valueAr) {
//                                        echo "<br /><br />";
//                                        echo $nameAr."  =>  ";
//                                        echo '<pre>' . var_export($valueAr, true) . '</pre>';
////                                        echo var_dump();
//                                }
//
//                                echo "<br /><br />";
//
//                            }
?>
<!--

						<div class='post-<?php echo esc_attr( $lesson['post']->ID ); ?> <?php echo esc_attr( $lesson['sample'] ); ?>'>

							<div class="list-count">
								<?php echo $lesson['sno']; ?>
							</div>

							<h4>
								<a class='<?php echo esc_attr( $lesson['status'] ); ?>' href='<?php echo esc_attr( $lesson['permalink'] ); ?>'><?php echo $lesson['post']->post_title; ?></a>


								<?php
                                /**
                                 * Not available message for drip feeding lessons
                                 */
                                ?>
								<?php if ( ! empty( $lesson['lesson_access_from'] ) ) : ?>
									<?php /* ?>
									<small class="notavailable_message">
										<?php echo sprintf( __( 'CCC Available on: %s ', 'learndash' ), learndash_adjust_date_time_display( $lesson['lesson_access_from'] ) ); ?>
										
									</small>
									<?php */ ?>
									<?php
										SFWD_LMS::get_template( 
											'learndash_course_lesson_not_available', 
											array(
												'user_id'					=>	$user_id,
												'course_id'					=>	learndash_get_course_id( $lesson['post']->ID ),
												'lesson_id'					=>	$lesson['post']->ID,
												'lesson_access_from_int'	=>	$lesson['lesson_access_from'],
												'lesson_access_from_date'	=>	learndash_adjust_date_time_display( $lesson['lesson_access_from'] ),
												'context'					=>	'course'
											), true
										);
									?>
								<?php endif; ?>


								<?php
                                /**
                                 * Lesson Topics
                                 */
                                ?>
								<?php $topics = @$lesson_topics[ $lesson['post']->ID ]; ?>

								<?php if ( ! empty( $topics ) ) : ?>
									<div id='learndash_topic_dots-<?php echo esc_attr( $lesson['post']->ID ); ?>' class="learndash_topic_dots type-list">
										<ul>
											<?php $odd_class = ''; ?>
											<?php foreach ( $topics as $key => $topic ) : ?>
												<?php $odd_class = empty( $odd_class ) ? 'nth-of-type-odd' : ''; ?>
												<?php $completed_class = empty( $topic->completed ) ? 'topic-notcompleted':'topic-completed'; ?>												
												<li class='<?php echo esc_attr( $odd_class ); ?>'>
													<span class="topic_item">
														<a class='<?php echo esc_attr( $completed_class ); ?>' href='<?php echo esc_attr( get_permalink( $topic->ID ) ); ?>' title='<?php echo esc_attr( $topic->post_title ); ?>'>
															<span><?php echo $topic->post_title; ?></span>
														</a>
													</span>
												</li>
											<?php endforeach; ?>
										</ul>
									</div>
								<?php endif; ?>

							</h4>
						</div>
-->
					<?php endforeach; ?>

				</div>
			</div>
			<?php
				if ( isset( $lessons_results['pager'] ) ) {
					echo '<div class="navigation" style="clear:both">';
					if ( $lessons_results['pager']['paged'] > 1 ) {
						if ( intval($lessons_results['pager']['paged']) == 2 )
							$prev_link = remove_query_arg( 'ld-lesson-page' );
						else 
							$prev_link = add_query_arg('ld-lesson-page', intval($lessons_results['pager']['paged'])-1 );
						echo '<div class="alignleft"><a href="'. $prev_link .'">'. __( '« Previous', 'learndash' ) .'</a></div>';
					}
					if ( $lessons_results['pager']['paged'] < $lessons_results['pager']['total_pages'] ) {
						echo '<div class="alignright"><a href="'. add_query_arg('ld-lesson-page', intval($lessons_results['pager']['paged'])+1 ) .'">'. __('Next »', 'learndash' ) .'</a></div>';
					}
					echo '</div>';
					echo '<div style="clear:both"></div>';
					
				}
			?>
			
			
		<?php endif; ?>


		<?php
        /**
         * Display quiz list
         */
        ?>
		<?php if ( ! empty( $quizzes ) ) : ?>
<!--
			<div id="learndash_quizzes" class="learndash_quizzes">
				<div id="quiz_heading">
						<span><?php echo LearnDash_Custom_Label::get_label( 'quizzes' ) ?></span><span class="right"><?php _e( 'Status', 'learndash' ); ?></span>
				</div>
				<div id="quiz_list" class=“quiz_list”>

					<?php foreach( $quizzes as $quiz ) : ?>
						<div id='post-<?php echo esc_attr( $quiz['post']->ID ); ?>' class='<?php echo esc_attr( $quiz['sample'] ); ?>'>
							<div class="list-count"><?php echo $quiz['sno']; ?></div>
							<h4>
                                <a class='<?php echo esc_attr( $quiz['status'] ); ?>' href='<?php echo esc_attr( $quiz['permalink'] ); ?>'><?php echo $quiz['post']->post_title; ?></a>
                            </h4>
						</div>						
					<?php endforeach; ?>

				</div>
			</div>
-->
		<?php endif; ?>

	</div>
		<?php endif; ?>
<?php endif; ?>
<!--

                                <script>
                                   $("#sfwd-mark-complete").submit(function() {
                                        alert("Are you sure?");
                                       return;
                                    });
                                   </script>
-->
