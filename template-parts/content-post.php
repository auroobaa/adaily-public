<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package ArabicDaily_Theme
 */

?>
<?php 

$button = get_field('articles_cta_button', 'option'); ?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php
			if ( is_single() ) {
				the_title( '<h1 class="entry-title">', '</h1>' );
			} ?>

		<?php if ( 'post' === get_post_type() ) : ?>
		<div class="entry-meta">
			<?php arabicdaily_posted_on(); ?>
		</div><!-- .entry-meta -->
		<?php
		endif; ?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php
			the_content( sprintf(
				/* translators: %s: Name of current post. */
				wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'arabicdaily' ), array( 'span' => array( 'class' => array() ) ) ),
				the_title( '<span class="screen-reader-text">"', '"</span>', false )
			) );

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'arabicdaily' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
        <div class="post-cta-text"><?php the_field('articles_cta_text', 'option'); ?></div>
        <?php if ( $button['url'] ) : ?> 
        <div class="button-wrapper">
            <a class="button button-ghost" href="<?php echo $button['url']; ?>" target="<?php echo $button['target']; ?>"><?php echo $button['text']; ?></a>
        </div>
        <?php endif; ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
