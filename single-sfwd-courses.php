<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package ArabicDaily_Theme
 */

get_header( 'course' ); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php
		while ( have_posts() ) : the_post();

			get_template_part( 'template-parts/content', 'courses' );

			the_post_navigation();

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
//$has_access = sfwd_lms_has_access( get_the_ID(), get_current_user_id() );
//if ( ( !is_user_logged_in() ) || ( is_user_logged_in() and !$has_access ) ) :
    get_footer( 'course' );
//elseif ( is_user_logged_in() and $has_access ) :
//    get_footer();
//endif;


