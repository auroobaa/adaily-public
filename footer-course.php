<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package ArabicDaily_Theme
 */

?>
<?php
$call_to_action_text = get_field('call_to_action_text', 'option');
$course_cta_heading = $call_to_action_text['course_cta_heading'];
$course_cta_text = $call_to_action_text['course_cta_text'];
$course_cta_button = get_field('course_cta_button', 'option');
$course_cta_buy_button = get_field('course_cta_buy_button', 'option');
$woocommerce_product = get_field('select_woocommerce_product');
$woocommerce_bundle = '';
if (get_field('is_there_a_pricing_page_for_this_product')) {
    $woocommerce_bundle = get_field('select_pricing_page');
    $woocommerce_bundle_text = get_field('buy_now_button_text');
}
$course_id = learndash_get_course_id();
$user_id = get_current_user_id();
$has_access = sfwd_lms_has_access( $course_id, $user_id );
?>
	</div><!-- #content -->
	<?php if (!$has_access) : ?>
	<footer id="course-colophon" class="course-footer" role="contentinfo">
        <div class="content-wrapper">
            <div class="course-curious">
                <h3><?php echo $course_cta_heading; ?></h3>
                <p class="course-cta"><?php echo $course_cta_text; ?></p>
            </div>
            <div class="course-start-button button-wrapper">
               <?php if (!is_user_logged_in()) { ?>
                        <a class="button button-blue-white white-border" href="<?php echo $course_cta_button['url']; ?>"><?php echo $course_cta_button['text']; ?></a>                
                    <?php } else { ?>
                        <a class="button button-blue-white white-border" href="<?php echo home_url(); ?>/?add-to-cart=<?php echo $woocommerce_product; ?>"><?php echo $course_cta_buy_button; ?></a>
                        <?php if (get_field('is_there_a_pricing_page_for_this_product')): ?>
                            <a class="colour-white bundle-text" href="<?php echo get_permalink($woocommerce_bundle); ?>"><?php echo $woocommerce_bundle_text; ?></a>
                        <?php endif; ?>
                    <?php } ?>
            </div>
        </div>
	</footer><!-- #colophon -->
	<?php endif; ?>
</div><!-- #page -->
<script>
    
    function openNav() {
        document.getElementById("site-navigation").style.right = "0px";
        document.getElementById("site-navigation").className = "main-navigation open";
    }

    function closeNav() {
        document.getElementById("site-navigation").style.right = "-100%";
        document.getElementById("site-navigation").className = "main-navigation";
    }
</script>
<script>
jQuery(".slide-header").hover(function () {
    jQuery(".slide-content").slideDown("slow");
}, function(){
    jQuery(".slide-content").slideUp("slow");
});
</script>

<script>
    var PI = { 
        onReady: function() {
            jQuery('.mobile-nav').click( PI.mobnav );  
        },
        
        mobnav: function(event) {
            jQuery('#mobile-site-navigation').slideToggle(500);
        },
        
        submenu: function(event) {
            jQuery('.main-navigation li.menu-item-has-children').hover( PI.showsubmenu );
        },
        
        showsubmenu: function(event) {
            jQuery(this).children('.sub-menu').slideToggle(200);
        }
        
    };
    
//    jQuery(document).ready(PI.onReady);
    jQuery(document).ready(PI.submenu);
    
</script> 

<script>
  (function(d) {
    var config = {
      kitId: 'yix8swy',
      scriptTimeout: 3000,
      async: true
    },
    h=d.documentElement,t=setTimeout(function(){h.className=h.className.replace(/\bwf-loading\b/g,"")+" wf-inactive";},config.scriptTimeout),tk=d.createElement("script"),f=false,s=d.getElementsByTagName("script")[0],a;h.className+=" wf-loading";tk.src='https://use.typekit.net/'+config.kitId+'.js';tk.async=true;tk.onload=tk.onreadystatechange=function(){a=this.readyState;if(f||a&&a!="complete"&&a!="loaded")return;f=true;clearTimeout(t);try{Typekit.load(config)}catch(e){}};s.parentNode.insertBefore(tk,s)
  })(document);
</script>

<?php wp_footer(); ?>

</body>
</html>
