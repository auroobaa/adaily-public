arabicdaily: a theme by Aurooba
===================================
#####Last updated: Mar 20, 2016

Hi. I'm a theme called `arabicdaily`. I was created by the Aurooba.

I am written in HTML, PHP, JS, and CSS pre-processed with SCSS.

Getting Started with Local Development
--------------------------------------

This theme requires Ruby, Node.js, Gulp.js, and Sass. If you're on a Mac, you already have Ruby. 

* Go to the [Node website](http://nodejs.org/) to download and install Node.js.

* Install gulp with this command in terminal: `npm install gulp -g`

* Install sass with this command in terminal: `gem install sass` if you get an error, use this command instead: `sudo gem install sass`

Using Gulp
----------

Tasks such as processing SCSS, minifying images, and updating files are automated with Gulp.js. To use gulp, you must first install node dependencies.

In terminal, use this command to install all dependencies 
```
#!bash
npm install
```
Use the command `gulp watch` in the root of the development directory to have gulp watch the theme for changes and update appropriately.

To carry out specific tasks, refer to `gulpfile.js`.

## Creating a symbolic link from your development folder to wordpress ##

`nuday` is set up to allow you to develop your theme in a separate folder outside of the `/themes` folder in your local Wordpress installation. To do this, carry out the following steps:

* Navigate to your development folder in terminal and use the command below to find the full URL to it, copy this URL
```
#!bash
pwd
```
* Navigate to the themes folder inside your local wordpress installation in terminal and type this command to create a symbolic link
```
#!bash
ln -s [paste URL of development folder] [name you want to give folder in themes]
```